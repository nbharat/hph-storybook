import { create } from '@storybook/theming';
import hutchisonportslogo from '../src/stories/assets/hutchisonportslogo.svg'

export default create({
    base: 'light',
    brandTitle: 'HPH-UI Components',
    brandUrl: 'https://ada.hph.com/bitbucket/projects/DEV/repos/hph-ui/browse/hph-component?at=refs%2Fheads%2Fdevelopment',
    brandImage: hutchisonportslogo,
});