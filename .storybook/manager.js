import { addons } from '@storybook/addons';
import hphTheme from "./hphTheme";

addons.setConfig({
    theme: hphTheme,
});

