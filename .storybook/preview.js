export const parameters = {
  layout:'centered',
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {expanded:true},
  options: {
    includeName:true,
      storySort: (a, b) => {
        
        const config = [
          {
            category: 'EXAMPLE',
            order: ['Introduction'],
          },
          {
            category: 'COMPONENT',
            // order: [
            // 'Layout Elements',
            // 'Form Elements',
            // 'Data Display',
            // 'Navigation Elements'
            // ],
          },
        //   {
        //     category: 'Layout Elements',
        //     order: [
        //     'Right Click Menu',
        //     'Container',
        //     'Overflowing Menu',
        //     'Overflowing Dialog',
        //     'Loading Indicators',
        //     'Dialog',
        //     'Dialog Without Modal',

        //     ],
        //   },
        //   {
        //     category: 'Form Elements',
        //     order: [
        //     'Slider',
        //     'Dropdown',
        //     'Input Field',
        //     'Time Picker',
        //     'Date Picker',
        //     'Date Range Picker',
        //     'Date Time Picker',
        //     'Check Box',
        //     'Toggle',
        //     'Radio Button',
        //     'Button',
        //     'Group Button',
        //     ],
        //   },
        //   {
        //     category: 'Data Display',
        //     order: [
        //   'Icon Button',
        //    'Avatar',
        //    'Icons',
        //    'Notification Card',
        //    'Notification Bar',
           
        //     ],
        //   },
        ];
      
        const story1 = a[1].kind.split('/');
        const story2 = b[1].kind.split('/');
      
       
        function getOrderNumber(needle, haystack) {
          let order = 9999;
          if (Array.isArray(haystack)) {
            order = haystack.findIndex(h => h.toLowerCase() === needle.toLowerCase());
            if (order === -1) order = 9999;
          }
          return order;
        }
      
        const topLevelOrderArray = config.map(h => h.category);
      
        const topLevelOrder1 = getOrderNumber(story1[0], topLevelOrderArray);
        const topLevelOrder2 = getOrderNumber(story2[0], topLevelOrderArray);
        
        const topLevelOrder3 = getOrderNumber(story1[1], topLevelOrderArray);
        const topLevelOrder4 = getOrderNumber(story2[1], topLevelOrderArray);
        if (story1[0] !== story2[0]) {
          return topLevelOrder1 - topLevelOrder2;
        }
        if (story1[1] !== story2[1]) {
          return getOrderNumber(story1[1], config[topLevelOrder1] && config[topLevelOrder1].order) - getOrderNumber(story2[1], config[topLevelOrder2] && config[topLevelOrder2].order)
        }
        if (story1[2] !== story2[2]) {
          return getOrderNumber(story1[2], config[topLevelOrder3] && config[topLevelOrder3].order) - getOrderNumber(story2[2], config[topLevelOrder4] && config[topLevelOrder4].order)
        }
      
        return 0;
      }
    },
  }

