import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
    title: "component/Data Display/Avatar",
    component: Core.HPHAvatar,
    argTypes: {
        size: {
            type: { name: 'string', required: false },
            defaultValue: 'large',
            description: 'Size of Avatar',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: [ "xSmall" , "small", "medium", "large" , "xLarge" , "xxLarge"]
            },
        },
        name: {      
            type: { name: 'string', required: false },
            defaultValue: "chan Kitty",
            description: 'Name of Avatar',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          }, 

          backgroundColor: {
            type: { name: 'string', required: false },
            defaultValue: '#002E6D',
            description: 'Color of Avatar',
            table: {
                type: { summary: 'color' },
            },
            control: {
                type: "color"
            },
        },

        showStatus: {
            type: { name: "boolean", required: false },
            defaultValue: true,
            description: "Show Status of Avatar",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
        
          status: {
            type: { name: 'string', required: false },
            defaultValue: 'online',
            description: 'Status of Avatar',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: ["away" , "online" , "busy" ]
            },
        },
        imageUrl: {
            type: { name: 'string', required: false },
            description: 'Select Image of Avatar',
            table: {
                type: { summary: 'file' },
            },
            control: {
                type: "file",
            },
        },
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'An avatar is an UI object that represents the users identity on a screen. The avatar usually contains a profile picture of the user.\n'+
              '<ul><li>The user’s status could be represented by a status dot, which sits at the bottom right of the avatar. The dot represents if the user is currently online or available.</li>\n'+
              '<li>For users who do not have a profile image uploaded, the avatar will be filled with his or her initials.</li>\n'+
              '<li>For avatars that contain only a character, the default background color is Ports Sea Blue. Users can be assigned other colors depending on the use case.</li>\n'
            }
        }
    },
} as ComponentMeta<typeof Core.HPHAvatar>;

const tempAvatar: ComponentStory<typeof Core.HPHAvatar> = (args) => (
    <Core.HPHAvatar {...args} />
);

export const Demo = tempAvatar.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };
Demo.args = {
};

export const Avatar = tempAvatar.bind({});
tempAvatar.args = {

};

