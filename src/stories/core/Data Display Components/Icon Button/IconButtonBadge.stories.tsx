
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
    title: "component/Data Display/Icon Button",
    component: Core.HPHBadge,
    argTypes: {
        
        value: {
            type: { name: 'number', required: false },
            defaultValue: '2',
            description: 'value of badge',
            table: {
                type: { summary: 'number' },
            },
            control: {
                type: "number"
            },
        },
        size: {
            type: { name: 'string', required: false },
            defaultValue: 'Badge-medium',
            description: 'size of icon button',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: ["Badge-small", "Badge-medium", "Badge-large"]
            },
        },
        color: {
            type: { name: 'string', required: false },
            defaultValue: 'Alert-Red',
            description: 'Color of Badge',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: ["Alert-Red", "Sky-Blue", "Sea-Blue", "Horizon-Blue", "Aqua-Green", "Sunray-Yellow", "Sunset-Orange"]
            },
        }
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'The badge is used to indicate counts of unread notifications \n'
            }
        }
    },
} as ComponentMeta<typeof Core.HPHBadge>;

const tempBadge: ComponentStory<typeof Core.HPHBadge> = (args) => (
    <Core.HPHBadge  {...args} />
);

export const Badge = tempBadge.bind({});
Badge.args = {

};



