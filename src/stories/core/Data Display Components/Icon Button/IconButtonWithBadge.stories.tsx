
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
  title: "component/Data Display/Icon Button",
  component: Core.IconButtonWithBadge, 
 
  argTypes: {
    fileName: {     
      name: "iconType",
      type: { name: 'string', required: false , },
      defaultValue: 'Icon-doc',
      description: 'icon button Name',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["Icon-doc", "Icon-docs-add", "Icon-docs-fill","Icon-docs-list"]
      },
    },
    size: {
      type: { name: 'string', required: false },
      defaultValue: 'medium',
      description: 'size of icon button',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["small", "medium", "large"]
      },
    },
    value: {
      type: { name: 'number', required: false },
      defaultValue: '2',
      description: 'value of badge',
      table: {
        type: { summary: 'number' },
      },
      control: {
        type: "number"
      },
    },
    toolTipPlacement: {
      type: { name: 'string', required: false },
      defaultValue: 'bottom',
      description: 'Tooltip Position',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["top", "right", "left", "bottom","topLeft","topRight","bottomLeft","bottomRight"]
      },
    },

    toolTipArrow: {
      type: { name: "boolean", required: false },
      defaultValue: true,
      description: "Give your tooltip an arrow indicating which element it refers to",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    enabledActive: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Enabled Active Button",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
  },
  parameters: {
    options: {showPanel: true },
    docs: {
      description: {
        component: 'Icon buttons are icons that are used as ‘buttons’ - which means that when users click on an icon button, it will lead users to take an action or make a choice.\nThe badge is used to indicate counts of unread notifications \n'
      }
    }
  },
} as ComponentMeta<typeof Core.IconButtonWithBadge>;

export const IconButtonWithBadge: ComponentStory<typeof Core.IconButtonWithBadge> = (args) => (
  <Core.IconButtonWithBadge {...args}
  />
);



