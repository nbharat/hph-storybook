import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
  title: "component/Data Display/Icons",
  component: Core.Icon,
  argTypes: {    
    // disabled: {
    //   type: { name: "boolean", required: false },
    //   defaultValue: false,
    //   description: "Disable Button",
    //   table: {
    //     type: { summary: "boolean" },
    //   },
    //   control: {
    //     type: "boolean",
    //   },
    // },
  },
} as ComponentMeta<typeof Core.Icon>;

const Template: ComponentStory<typeof Core.Icon> = (args) => (
  <Core.Icon {...args} />
);
export const Icons = Template.bind({});
Icons.args = {  
  //alt:"icon",
};
