import { ComponentStory, ComponentMeta } from "@storybook/react";
import {LozengesComponent} from '../../../pages/Lozenges.page';


export default {
  title: "component/Data Display/Lozenges",
  component: LozengesComponent,
  argTypes: {
    variation: {
      type: { name: 'string', required: false },
      defaultValue: 'Ports Horizon Blue',
      description: 'Variation of Lozenges',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "select",
          options: [ 'Ports Sea Blue' , 'Ports Sky Blue' , 'Ports Horizon Blue', 'Ports Aqua Green', 'Ports Sunray Yellow', 'Ports Sunset Orange', 'Alert Red' ]
        },
    },
    label: {  
      type: { name: 'string', required: false },
      defaultValue: "Default",
      description: 'label for Lozenges',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
    enableTooltip: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Enable the tooltip",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    toolTipPlacement: {
      type: { name: 'string', required: false },
      defaultValue: 'bottom',
      description: 'Tooltip Position',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["top", "right", "left", "bottom","topLeft","topRight","bottomLeft","bottomRight"]
      },
    },
    toolTiptext: {  
      type: { name: 'string', required: false },
      defaultValue: "Default Tooltip",
      description: 'Tooltip text for Lozenges',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
    toolTipArrow: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Give your tooltip an arrow indicating which element it refers to",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    }
  },
  parameters: {
    options: {showPanel: true },
    docs: {
      description: {
        component: 'A lozenge is used to highlight an item’s status for users’ quick identification, or to provide extra attention-needed information for instant consumption. Lozenges are usually of small/ subtle appearances, thus colors are important factors that provide meaning to the lozenges.  \n'  
      },           
    },
  },
} as ComponentMeta<typeof LozengesComponent>;

const tempLozenges: ComponentStory<typeof LozengesComponent> = (args) => (
  <LozengesComponent {...args} />
);

export const Demo = tempLozenges.bind({});
Demo.parameters= {
  options: {showPanel: false },
};


export const Lozenges = tempLozenges.bind({});
tempLozenges.args = {

};
