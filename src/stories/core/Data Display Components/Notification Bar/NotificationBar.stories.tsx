import { ComponentStory, ComponentMeta } from "@storybook/react";
import Notification from '../../../pages/NoticationBar.page';


export default {
  title: "component/Data Display/Notification Bar",
  component: Notification,
  argTypes: {
    width: {
      type: { name: 'string', required: false },
      defaultValue: "750px",
      description: 'Width of the Notification Bar',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      }
    },
    height: {
      type: { name: 'string', required: false },
      defaultValue: "70px",
      description: 'Height of the Notification Bar',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        }
    },
    background: {
      type: { name: 'string', required: false },
      defaultValue: 'Alert Red',
      description: 'Background color of Notification Bar',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "select",
          options: [ 'Alert Red' , 'Warning Sunset Orange' , 'Success Aqua Green' ]
        },
    },
    text: {  
      type: { name: 'string', required: false },
      defaultValue: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, nihil rem dolo!",
      description: 'Text in Notification Bar',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
    disable: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled Time",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    timer: {
      type: { name: 'number', required: false },
      defaultValue: '20',
      description: 'Timer for Notification bar',
      table: {
          type: { summary: 'number' },
      },
      control: {
          type: "number"
      },
  },
  },
  parameters: {
    options: {showPanel: true },
    docs: {
      description: {
        component: 'A notification bar is a variant of `notifications`.The notification bar is designed to host notification messages that aim to communicate attention-needed information to users. notification bars tend to appear with higher prominence - it usually comes with a countdown to urge users. Notifications could be similar to alerts in some cases thus the alert/ notification content needs to be precise & to-the-point.  \n'  
      },           
    },
  },
} as ComponentMeta<typeof Notification>;

const tempBar: ComponentStory<typeof Notification> = (args) => (
  <Notification {...args} />
);

export const Demo = tempBar.bind({});
Demo.parameters= {
  options: {showPanel: false },
};


export const NotificationBar = tempBar.bind({});
tempBar.args = {

};
