import { ComponentStory, ComponentMeta } from "@storybook/react";
import { NotificationToast }from '../../../pages/NoticationCard.page'

export default {
    title: "component/Data Display/Notification Card",
    component: NotificationToast,
    argTypes: {
        width: {
            name: "width",
            type: { name: 'string', required: false },
            defaultValue: "400px",
            description: 'Width of Notification card',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "text"
            }
          },
          height: {
            name: "height",
            type: { name: 'string', required: false },
            defaultValue: "auto",
            description: 'Height of Notification card',
            table: {
                type: { summary: 'string' },
            },
            control: { 
                type: "text"
            }
          },
          heading: {  
            type: { name: 'string', required: false },
            defaultValue: "Alert Issue 1",
            description: 'Label of Notification card',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          text: {  
            type: { name: 'string', required: false },
            defaultValue: "Lorem ipsum dolor sit amet cons, adipisicing elit. Enim, nihil rem!",
            description: 'Text in Notification card',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          disabledIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Icon in Card",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          disabledButton: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable buttons",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          disabledLabel: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Label",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A Notification Card serves to contain notification messages that aim to communicate attention-needed information to users. Notifications could be similar to alerts in some cases thus the alert/ notification content needs to be precise & to-the-point. '
            }
        }
    },
} as ComponentMeta<typeof NotificationToast>;

const tempCard: ComponentStory<typeof NotificationToast> = (args) => (
    <NotificationToast {...args} />
);

export const Demo = tempCard.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };
Demo.args = {
};

export const NotificationCard = tempCard.bind({});
tempCard.args = {

};

