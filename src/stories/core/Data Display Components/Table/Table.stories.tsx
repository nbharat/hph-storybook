import { ComponentStory, ComponentMeta } from "@storybook/react";
import { object } from '@storybook/addon-knobs';
import { TableComponent } from '../../../pages/Table.page';

const Component = TableComponent;

const data = [
    {
        "id": 1000,
        "name": "James Butt",
        "country":"Algeria",
        "company": "Benton, John B Jr",
        "dateTime": "2017-05-20 06:18",
        "status": "unqualified",
        "verified": true,
        "color":"#009BDE",
        "activity": 17,
        "representative": {
            "name": "Ioni Bowcher",
            "image": "ionibowcher.png"
        },
        "balance": 70663
    },
    {
        "id": 1001,
        "name": "Josephine Darakjy",
        "country": "Egypt",
        "company": "Chanay, Jeffrey A Esq",
        "dateTime": "2020-09-15 20:03",
        "status": "proposal",
        "verified": true,
        "color":"#54BBAB",
        "activity": 0,
        "representative": {
            "name": "Amy Elsner",
            "image": "amyelsner.png"
        },
        "balance": 82429
    },
    {
        "id": 1002,
        "name": "Art Venere",
        "country": "Panama",
        "company": "Chemel, James L Cpa",
        "dateTime": "2007-10-21 15:30",
        "status": "qualified",
        "verified": false,
        "color":"#FFC627",
        "activity": 63,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 28334
    },
    {
        "id": 1003,
        "name": "Lenna Paprocki",
        "country":"Slovenia",
        "company": "Feltz Printing Service",
        "dateTime": "2017-05-20 05:18",
        "status": "new",
        "verified": false,
        "color":"#EE7523",
        "activity": 37,
        "representative": {
            "name": "Xuxue Feng",
            "image": "xuxuefeng.png"
        },
        "balance": 88521
    },
    {
        "id": 1004,
        "name": "Donette Foller",
        "country":"South Africa",
        "company": "Printing Dimensions",
        "dateTime": "2017-05-20 06:20",
        "status": "proposal",
        "verified": true,
        "color":"#DB4D59",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1005,
        "name": "Donett Foller",
        "country":"South Africa",
        "company": "Printing Dimensions",
        "dateTime": "2012-06-10 06:18",
        "status": "proposal",
        "verified": true,
        "color":"#F5CCD0",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1006,
        "name": "Donett Foll",
        "country":"South Africa",
        "company": "Printing Dimensions",
        "dateTime": "2021-12-20 06:18",
        "status": "proposal",
        "verified": true,
        "color":"#AB4C0D",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1007,
        "name": "Gaming Set",
        "country":"Slovenia",
        "company": "Printing Dimensions",
        "dateTime": "2011-01-10 03:02",
        "status": "proposal",
        "verified": true,
        "color":"#FBDBC6",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1008,
        "name": "Aabel",
        "country":"Afghanistan",
        "company": "Printing Dimensions",
        "dateTime": "2013-11-01 04:20",
        "status": "proposal",
        "verified": true,
        "color":"#222222",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1009,
        "name": "Aabheer",
        "country":"Cambodia",
        "company": "Printing Dimensions",
        "dateTime": "2019-02-12 01:03",
        "status": "proposal",
        "verified": true,
        "color":"#FF0000",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1010,
        "name": "Yuze Tao",
        "country":"Canada",
        "company": "Printing Dimensions",
        "dateTime": "2008-06-01 02:08",
        "status": "proposal",
        "verified": true,
        "color":"#FBDBC6",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1011,
        "name": "Hao-Yu",
        "country":"China",
        "company": "Printing Dimensions",
        "dateTime": "2019-10-10 16:08",
        "status": "proposal",
        "verified": true,
        "color":"#FFEFC2",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1012,
        "name": "Da-Fu",
        "country":"Egypt",
        "company": "Printing Dimensions",
        "dateTime": "2011-11-22 00:08",
        "status": "proposal",
        "verified": true,
        "color":"#D3EEEA",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1013,
        "name": "Chih-Cheng",
        "country":"Germany",
        "company": "Printing Dimensions",
        "dateTime": "2017-02-28 16:08",
        "status": "proposal",
        "verified": true,
        "color":"#C5E0F2",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1014,
        "name": "Taio Shan",
        "country":"South Africa",
        "company": "Printing Dimensions",
        "dateTime": "2001-02-10 10:17",
        "status": "proposal",
        "verified": true,
        "color":"#C2EDFF",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1015,
        "name": "Ming Lim",
        "country":"Italy",
        "company": "Printing Dimensions",
        "dateTime": "2015-11-04 03:18",
        "status": "proposal",
        "verified": true,
        "color":"#0072A3",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1016,
        "name": "Kong Jing",
        "country":"Nepal",
        "company": "Printing Dimensions",
        "dateTime": "2011-10-10 04:08",
        "status": "proposal",
        "verified": true,
        "color":"#C5E0F2",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1017,
        "name": "Jiao-Long",
        "country":"Russia",
        "company": "Printing Dimensions",
        "dateTime": "2012-02-06 06:28",
        "status": "proposal",
        "verified": true,
        "color":"#002E6D",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1018,
        "name": "Hong Fa",
        "country":"Singapore",
        "company": "Printing Dimensions",
        "dateTime": "2001-02-12 11:58",
        "status": "proposal",
        "verified": true,
        "color":"#FFEFC2",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1019,
        "name": "Dingbang",
        "country":"Singapore",
        "company": "Printing Dimensions",
        "dateTime": "2011-09-05 02:48",
        "status": "proposal",
        "verified": true,
        "color":"#FBDBC6",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
    {
        "id": 1020,
        "name": "Li Jun",
        "country":"South Korea",
        "company": "Printing Dimensions",
        "dateTime": "2016-10-22 00:08",
        "status": "proposal",
        "verified": true,
        "color":"#ff0000",
        "activity": 33,
        "representative": {
            "name": "Asiya Javayant",
            "image": "asiyajavayant.png"
        },
        "balance": 93905
    },
];

const columns = [
    { id: 1,order: 1, field: 'name', header: 'Name',editable:true,editorType:'text', dataType:'text',disabled:true, sortable: true, filter: true, showFilterMatchModes: false, filterPlaceholder: 'Enter Name...', showAddButton: false, showFilterMenuOptions: false, showClearButton: false },
    { id: 2,order: 2, field: 'country', header: 'Country',editable:true,editorType:'select', dataType:'text',disabled: false, sortable: true, filter: true, showFilterMatchModes: false, filterPlaceholder: 'Enter Country...', showAddButton: false, showFilterMenuOptions: false, showClearButton: false },
    { id: 3,order: 3, field: 'dateTime', header: 'Date/Time',editable:true,editorType:'dateTime',dataType:'date', disabled: false,  sortable: true, filter: true, showFilterMatchModes: false, filterPlaceholder: 'Enter Date...', showAddButton: false, showFilterMenuOptions: false, showClearButton: false },
    { id: 4,order: 4, field: 'color', header: 'Color',editable:false,dataType:'color', disabled: false,  sortable: false, filter: true, showFilterMatchModes: false, filterPlaceholder: 'Enter Color...', showAddButton: false, showFilterMenuOptions: false, showClearButton: false },

    ];

export default {
    title: "component/Data Display/Table",
    component: Component,
    argTypes: {
        data: {
            name: "data",
            defaultValue: object("data",data),
            description: "Button options",
            table: {
              type: { summary: "Array[Object]" },
            },
        }, 
        columns: {
            name: "columns",
            defaultValue: object("columns",columns),
            description: "Button options",
            table: {
              type: { summary: "Array[Object]" },
            },
        },
        rows: {
            type: { name: 'number', required: false },
            defaultValue: '10',
            description: 'number of rows in a page',
            table: {
                type: { summary: 'number' },
            },
            control: {
                type: "number"
            },
        },
        editMode: {
            type: { name: 'string', required: false },
            defaultValue: 'cell',
            description: 'Select edit mode',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: [ "cell", "row" ]
            },
        },
        editable: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: " Enable both cell and row editing",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
        selectionMode: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Enable Multiple Select",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
        },
        showPaginator: {
            type: { name: "boolean", required: false },
            defaultValue: true,
            description: "Enable Pagination",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
        },
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'Tables are used to display a set of data in the format of rows and columns. A table helps to logically structure content in grids for users to view, understand, and compare. The column headers can sort data in ascending/ descending order. This table has 3 type of editors - text editor, select editor,and dateTime editor.'
            },
        },
        layout: 'fullscreen',
        backgrounds: {
            default: 'White',
            values: [
              { name: 'White', value: '#ffffff' },
              { name: 'Light grey', value: '#f4f4f4' },
              { name: 'Light Blue', value: '#f6f9fc' },
              { name: 'Dark grey', value: '#CCCCCC' },
            ],
          },
    },
} as ComponentMeta<typeof Component>;

const tempTable: ComponentStory<typeof Component> = (args) => (
    <Component {...args} />
);

export const Demo = tempTable.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };
Demo.args = {
};

export const Table = tempTable.bind({});
Table.args = {
    data: object("data",data),
    columns: object("columns",columns),
};
