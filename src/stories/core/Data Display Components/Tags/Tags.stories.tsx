import { ComponentStory, ComponentMeta } from "@storybook/react";
import {TagsDemoComponent} from '../../../pages/TagsDemo.page';
import {TagsComponent} from '../../../pages/Tags.page';


export default {
  title: "component/Data Display/Tags",
  component: TagsComponent,
  argTypes: {
    theme: {
      type: { name: 'string', required: false },
      defaultValue: 'Ports Sea Blue',
      description: 'Variation of Lozenges',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "select",
          options: [ 'Ports Sea Blue' , 'Ports Sky Blue', 'Ports Aqua Green', 'Ports Sunray Yellow', 'Ports Sunset Orange', 'Alert Red' ]
        },
    },
    label: {  
      type: { name: 'string', required: false },
      defaultValue: "Default",
      description: 'label for Tags',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
    width: {  
      type: { name: 'string', required: false },
      defaultValue: "auto",
      description: 'Width for Tags',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
    rounded: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Enable/Disable rounded corners",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    enableIcon: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Enable/Disable Icon",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    icon: {
      type: { name: 'string', required: false },
      defaultValue: 'Icon-doc',
      description: 'Define Icon for the Tag',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "select",
          options: ["Icon-doc", "Icon-tick"]
      },
    },
    
    remove: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Enable/Disable remove Tag functionality",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    onRemove: {
      description: "To handle the event when clicking on Remove. This functions is accessible when remove property is enabled.",
      action: 'click'
    },
    draggable: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Enable/Disable draggable Tag functionality",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    }
  },
  parameters: {
    options: {showPanel: true },
    docs: {
      description: {
        component: 'A tag is a small component that is used to label, categorize and/ or organize different items with keywords that describe them. Tags are often seen with components that allow selection & filtering, for example, in a dropdown menu, or in filters used in data tables. \n'  
      },           
    },
  },
} as ComponentMeta<typeof TagsComponent>;

const tempTags: ComponentStory<typeof TagsComponent> = (args) => (
  <TagsComponent {...args} />
);

const tempDemoTags: ComponentStory<typeof TagsDemoComponent> = (args) => (
  <TagsDemoComponent />
);

export const Demo = tempDemoTags.bind({});
Demo.parameters= {
  options: {showPanel: false },
};


export const Tags = tempTags.bind({});
tempTags.args = {

};
