import { ComponentStory, ComponentMeta } from "@storybook/react";
import { TooltipsComponent } from "../../../pages/Tooltips.page";

export default {
  title: "component/Data Display/Tooltips",
  component: TooltipsComponent,
  argTypes: {
    toolTipPlacement: {
      type: { name: 'string', required: false },
      defaultValue: 'bottom',
      description: 'Tooltip Position',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["top", "right", "left", "bottom","topLeft","topRight","bottomLeft","bottomRight"]
      },
    },
    toolTiptext: {  
      type: { name: 'string', required: false },
      defaultValue: "Default Tooltip",
      description: 'Tooltip text',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
    toolTipArrow: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Give your tooltip an arrow indicating which element it refers to",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    }
  },
  parameters: {
    options: {showPanel: true },
    docs: {
      description: {
        component: 'A tooltip is used to display supplementary, additional information upon clicking, hovering or focusing on an interactable element. It is a message box itself, though due to its nature, the message it carries should be short, neat and precise. A tooltip is a ‘consequence’ - it only appears when the users first perform a hover action on an interactable element.  \n'  
      },           
    },
  },
} as ComponentMeta<typeof TooltipsComponent>;

const tempTooltips: ComponentStory<typeof TooltipsComponent> = (args) => (
  <TooltipsComponent {...args} />
);

export const Demo = tempTooltips.bind({});
Demo.parameters= {
  options: {showPanel: false },
};


export const Tooltip = tempTooltips.bind({});
tempTooltips.args = {

};
