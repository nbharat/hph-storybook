import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
    title: "component/Form Elements/Button",
    component: Core.HPHButton,
    argTypes: {
        theme: {
            name: "theme",
            type: { name: 'string', required: false },
            defaultValue: 'Primary',
            description: 'Button Theme',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: ["Primary","Secondary","Alert"]
            },
          },
        size: {
            type: { name: 'string', required: false },
            defaultValue: 'Standard',
            description: 'Size of Button',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: [ "Standard","Small"]
            },
        },
        label: {  
            type: { name: 'string', required: false },
            defaultValue: "Button",
            description: 'Label of Button',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "disable the button",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          showIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "show the icon",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          icon: {  
            type: { name: 'string', required: false },
            defaultValue: 'Icon-add',
            description: 'Icon-add',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "select",
              options: [ "Icon-add","Icon-cross","Icon-alert","Icon-tick"]
            },
          },
          disableBadge: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "disable badge",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          badgeSize: {
            type: { name: 'string', required: false },
            defaultValue: 'Badge-large',
            description: 'Size of Badge',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: [ "Badge-small","Badge-medium","Badge-large"]
            },
        },
          badgeValue: {
            type: { name: 'number', required: false },
            defaultValue: '2',
            description: 'value of badge',
            table: {
              type: { summary: 'number' },
            },
            control: {
              type: "number"
            },
          },
          badgeColor: {
            type: { name: 'string', required: false },
            defaultValue: 'Alert-Red',
            description: 'color of badge',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "select",
              options: ["Alert-Red", "Sky-Blue", "Sea-Blue","Horizon-Blue", "Aqua-Green", "Sunray-Yellow","Sunset-Orange"]
            },
          },
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'Buttons are clickable elements that are used to initialize actions.There are different variations of button types in the Veronica system, which includes primary, secondary and alert; and each of them have different size/ format, which includes standard, and small. '
            }
        }
    },
} as ComponentMeta<typeof Core.HPHButton>;

const tempButton: ComponentStory<typeof Core.HPHButton> = (args) => (
    <Core.HPHButton {...args} />
);

export const Demo = tempButton.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };
Demo.args = {
};

export const Button = tempButton.bind({});
tempButton.args = {

};