import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Check } from "../../../pages/CheckBox.page";

const Component = Check;

export default {
  title: "component/Form Elements/Check Box",
  component: Component,
    argTypes: {
      disabled: {
        type: { name: "boolean", required: false },
        defaultValue: false,
        description: "disabled Active",
        table: {
          type: { summary: "boolean" },
        },
        control: {
          type: "boolean",
        },
      },
      errorMessage: {  
        type: { name: 'string', required: false },
        defaultValue: "",
        description: 'errorMessage of check box',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
      label: {  
        type: { name: 'string', required: false },
        defaultValue: "CheckBox",
        description: 'label of check box',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
    },
    parameters: {
      options: {showPanel: true },
      docs: {
        description: {
          component: 'Checkboxes are designed for users to tick and indicate their choices when there are multiple options in a list. If there is a group of checkboxes under a same category, a group label can be added.Checkbox labels should always be positioned at the checkbox inputs right hand side. \n'  
        },
      },
    },
} as ComponentMeta<typeof Component>;

const tempCheck: ComponentStory<typeof Component> = (args) => (
  <Component {...args} />
);

export const Demo = tempCheck.bind({});
Demo.parameters= {
  options: {showPanel: false },
};
Demo.args = {
};

export const checkBox = tempCheck.bind({});
tempCheck.args = {

};
