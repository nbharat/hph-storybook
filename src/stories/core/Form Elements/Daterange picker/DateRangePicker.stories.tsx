
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { DateRangePickerComponent } from "../../../pages/DateRangePicker.page";

export default {
    title: "component/Form Elements/Date Range Picker",
    component: DateRangePickerComponent,
    argTypes: {        
      label: {      
        type: { name: 'string', required: false },
        defaultValue: "label",
        description: 'label of Date Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
      width: {      
        type: { name: 'string', required: false },
        defaultValue: "300px",
        description: 'label of Input Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
        errorMessage: {  
          type: { name: 'string', required: false },
          defaultValue: "",
          description: 'errorMessage of Input Field date',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },           
      
        helpIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "helpIcon with tooltip-boolean property",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          toolTipText: {  
            type: { name: 'string', required: false },
            defaultValue: 'Date format dd/mm/yyyy',
            description: 'Date format dd/mm/yyyy',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },    
        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field date",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          required: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field date",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A Field that allows users to input date range manually.'
            }
        }
    },
} as ComponentMeta<typeof DateRangePickerComponent>;

const tempDateRangePicker: ComponentStory<typeof DateRangePickerComponent> = (args) => (
    <DateRangePickerComponent  {...args} />
);

export const Demo = tempDateRangePicker.bind({});
Demo.parameters= {
  options: {showPanel: false },
  layout: 'padded'
};
Demo.args = {};

export const DateRangePicker = tempDateRangePicker.bind({});
DateRangePicker.parameters = {
  layout: 'padded'
}
tempDateRangePicker.args = {};



