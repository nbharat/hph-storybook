
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { DateTimePickerComponent } from "../../../pages/DateTimePicker.page";

export default {
    title: "component/Form Elements/Date Time Picker",
    component: DateTimePickerComponent,
    argTypes: {        
      label: {      
        type: { name: 'string', required: false },
        defaultValue: "label",
        description: 'label of Date Field date time',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
        errorMessage: {  
          type: { name: 'string', required: false },
          defaultValue: "",
          description: 'errorMessage of Input Field date time',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        }, 
        helpIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "helpIcon with tooltip-boolean property",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          width: {      
            type: { name: 'string', required: false },
            defaultValue: "250px",
            description: 'label of Input Field',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          toolTipText: {  
            type: { name: 'string', required: false },
            defaultValue: 'Date time format dd/mm/yyyy hh:mm',
            description: 'Date time format dd/mm/yyyy hh:mm',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },    
        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable of Input date time",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          required: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field date time",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },    
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A Field that allows users to input a date and time manually.'
            }
        }
    },
} as ComponentMeta<typeof DateTimePickerComponent>;

const tempDateTimePicker: ComponentStory<typeof DateTimePickerComponent> = (args) => (
    <DateTimePickerComponent  {...args} />
);

export const Demo = tempDateTimePicker.bind({});
Demo.parameters= {
  options: {showPanel: false },
  layout: 'padded'
};
Demo.args = {};

export const DateTimePicker = tempDateTimePicker.bind({});
DateTimePicker.parameters = {
  layout: 'padded'
}
tempDateTimePicker.args = {};



