
import { ComponentStory, ComponentMeta } from "@storybook/react";
import InputDropdownDemo from "../../../pages/InputDropdownDemo";
import { object } from '@storybook/addon-knobs';
const Component = InputDropdownDemo;


const options = Array.from({ length: 50 })
.map((_, i) => ({ 
  dropdownLabel: `Option ${i+1}`,
  value: `option${i+1}`,
  tagLabel: `O${i+1}`,
  ...i % 2 === 0 ? {icon: 'Icon-doc'} : {},
  ...i === 0 ? { isMaster: true, lozengesLabel: `Default ${i+1}`, lozengesVariation: 'Ports Sea Blue' } : {}
}));

const virtualScrollerOptions = {
  showLoader: true,
  delay: 0,
  lazy: false
}


export default {
  title: "component/Form Elements/Dropdown",
  component: Component,
  argTypes: {
    options: {
      name: "options",
      defaultValue: object("options",options),
      description: "MenuItems for the Input Dropdown",
      table: {
        type: { summary: "Array[Object]" },
      },
    },      
    freeTextInput: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Enable/Disable freeText",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    mode: {
      type: { name: 'string', required: false },
      defaultValue: 'single',
      description: 'Dropdown mode Single',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["single"]
      },
    },
    width: {
      name: "width",
      type: { name: 'string', required: false },
      defaultValue: "250px",
      description: 'Width of the Input Dropdown',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "text"
      }
    },
    sort: {
      type: { name: "boolean", required: false },
      defaultValue: true,
      description: "Sort the options",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    field: {
      type: { name: 'string', required: false },
      defaultValue: 'dropdownLabel',
      description: 'Dropdown field to display selected item',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["dropdownLabel", "tagLabel", "value"]
      },
    },
    virtualScrollerOptions: {
      name: "virtualScrollerOptions",
      defaultValue: object("virtualScollerOptions",virtualScrollerOptions),
      description: "VirtualScroller options",
      table: {
        type: { summary: "VirtualScrollerOptions" },
      },
    },
   
    onChange: {
      action: 'change'
    }
  },
  parameters: {
    options: {showPanel: false },
    docs: {
      description: {
        component: 'A dropdown presents a list of options where users can choose one, or several from it. The chosen option(s) can represent a specific value in a form, or can be used as an actionable criteria to filter or sort (narrow down) existing content. '
      }
    }
  },
} as ComponentMeta<typeof Component>;

const comp: ComponentStory<typeof Component> = (args) => (
  <Component {...args}
  />
);

export const Demo = comp.bind({});

Demo.args = {
  options: object("options", options),
  virtualScrollerOptions: object("virtualScrollerOptions", virtualScrollerOptions)
};
