
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { object } from '@storybook/addon-knobs';
import { Core } from "veronica-ui-component";

const options = Array.from({ length: 50 }).map((_, i) => ({ dropdownLabel: `Option ${i+1}`, value: `option${i+1}`, tagLabel: `O${i+1}`, icon: 'Icon-doc' }));

const virtualScrollerOptions = {
  showLoader: true,
  delay: 0,
  lazy: false
}


export default {
  title: "component/Form Elements/Dropdown",
  component: Core.InputDropdown,
  argTypes: {
    options: {
      name: "options",
      defaultValue: object("options",options),
      description: "MenuItems for the Input Dropdown",
      table: {
        type: { summary: "Array[Object]" },
      },
    },      
    freeTextInput: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Enable/Disable freeText",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    mode: {
      type: { name: 'string', required: false },
      defaultValue: 'multiple',
      description: 'Dropdown mode Multiple',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["multiple"]
      },
    },
    width: {
      name: "width",
      type: { name: 'string', required: false },
      defaultValue: "250px",
      description: 'Width of the Input Dropdown',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "text"
      }
    },
    inputType: {
      type: { name: 'string', required: false },
      defaultValue: 'freeText',
      description: 'Dropdown inputType',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["freeText", "number", "text"]
      },
    },
    field: {
      type: { name: 'string', required: false },
      defaultValue: 'dropdownLabel',
      description: 'Dropdown field to display selected item',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["dropdownLabel", "tagLabel", "value"]
      },
    },
    virtualScrollerOptions: {
      name: "virtualScrollerOptions",
      defaultValue: object("virtualScollerOptions",virtualScrollerOptions),
      description: "VirtualScroller options",
      table: {
        type: { summary: "VirtualScrollerOptions" },
      },
    },
    errorMessage: {  
      type: { name: 'string', required: false },
      defaultValue: "",
      description: 'errorMessage of Input Field',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
    label: {      
      type: { name: 'string', required: false },
      defaultValue: "label",
      description: 'label of Input Field',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
    disabled: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disable Input Field",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    // sort: {
    //   type: { name: "boolean", required: false },
    //   defaultValue: false,
    //   description: "Sort the options",
    //   table: {
    //     type: { summary: "boolean" },
    //   },
    //   control: {
    //     type: "boolean",
    //   },
    // },
    onChange: {
      action: 'change'
    }
  },
  parameters: {
    options: {showPanel: true },
    docs: {
      description: {
        component: 'A Multi-Select dropdown presents a list of options where users can choose one, or several from it. The multi-select dropdown allows users to select more than one option from the menu. The selected option will be displayed in the field as a ‘tag’ with a ‘cross’ icon on the right, users can clear the option with a click on the ‘cross’ icon. Also, the selected values will also be highlighted in the menu, in order to indicate they’ve also been chosen.\n'
      }
    }
  },
} as ComponentMeta<typeof Core.InputDropdown>;

const comp: ComponentStory<typeof Core.InputDropdown> = (args) => (
  <Core.InputDropdown {...args}
  />
);

export const MultiSelectWithFreeTextInput = comp.bind({});

MultiSelectWithFreeTextInput.args = {
  options: object("options", options),
  virtualScrollerOptions: object("virtualScrollerOptions", virtualScrollerOptions)
};



