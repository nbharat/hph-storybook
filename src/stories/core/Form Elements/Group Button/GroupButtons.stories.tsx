import { ComponentStory, ComponentMeta } from "@storybook/react";
import { object } from '@storybook/addon-knobs';
import { GroupedButtonComponent } from '../../../pages/GroupButton.page';

const Component = GroupedButtonComponent;

const ButtonList=[
  {
    icon: 'Icon-cwp-hatch',
    name: 'Option 1 shows here',
    value: '1'
  },
  {
    icon: 'Icon-cwp-bay',
    name: 'Option 2',
    value: '2'
  },
  {
    icon: 'Icon-cwp-hatchSection',
    name: 'Option 3',
    value: '3'
  }
];
export default {
    title: "component/Form Elements/Group Button",
    component: Component,
    argTypes: {
        list: {
            name: "list",
            defaultValue: object("list",ButtonList),
            description: "Button options",
            table: {
              type: { summary: "Array[Object]" },
            },
          },  
          label: {  
            type: { name: 'string', required: false },
            defaultValue: "Group Button",
            description: 'label of Group Button',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          showIcon: {
            type: { name: "boolean", required: false },
            defaultValue: true,
            description: "Show Icon",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "disable the button",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          multiple: {
            type: { name: "boolean", required: false },
            defaultValue: true,
            description: "Select multiple buttons",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          errorMessage: {  
            type: { name: 'string', required: false },
            defaultValue: "",
            description: 'errorMessage of check box',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'Group Button is a component that refers to a group of choices simply. The choices in a Group Button are all equally emphasized. Group Buttons is a group of box-shaped options with behaviors like buttons with both multiple selection and single selection allowed. '
            }
        }
    },
} as ComponentMeta<typeof Component>;

const tempGroupedButtons: ComponentStory<typeof Component> = (args) => (
    <Component {...args} />
);

export const Demo = tempGroupedButtons.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };


export const GroupButton = tempGroupedButtons.bind({});
GroupButton.args = {
  list: object("list",ButtonList),
};