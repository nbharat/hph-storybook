
import { ComponentStory, ComponentMeta } from "@storybook/react";
import InputFieldDemo from "../../../pages/InputField.page"; 

const Component = InputFieldDemo;

export default {
    title: "component/Form Elements/Input Field",
    component: Component,
    argTypes: {        
        type: {
            type: { name: 'string', required: false },
            defaultValue: 'text',
            description: 'type of Input Field',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: ["number", "text"]
            },
        },
        errorMessage: {  
          type: { name: 'string', required: false },
          defaultValue: "",
          description: 'errorMessage of Input Field',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },
          
        label: {      
          type: { name: 'string', required: false },
          defaultValue: "label",
          description: 'label of Input Field',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },
        width: {      
          type: { name: 'string', required: false },
          defaultValue: "200px",
          description: 'label of Input Field',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },
        helpIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "helpIcon with tooltip-boolean property",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          toolTipText: {  
            type: { name: 'string', required: false },
            defaultValue: 'Help Icon',
            description: 'toolTipText of text area',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },    
        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          required: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Required Input Field",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          
    },
    parameters: {
        options: {showPanel: false},
    },

} as ComponentMeta<typeof Component>;

export const Demo: ComponentStory<typeof Component> = (args) => (
    <Component  {...args} />
);



