import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";



export default {
  title: "component/Form Elements/Input Field",
  component: Core.HPHInputMask,
  argTypes: {
    label: {      
        type: { name: 'string', required: false },
        defaultValue: "Co/Vsl/Voy",
        description: 'label of Input Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      }, 
      width: {      
        type: { name: 'string', required: false },
        defaultValue: "300px",
        description: 'label of Input Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
      disabled: {
        type: { name: "boolean", required: false },
        defaultValue: false,
        description: "Disable Input Field",
        table: {
          type: { summary: "boolean" },
        },
        control: {
          type: "boolean",
        },
      },          
      required: {
        type: { name: "boolean", required: false },
        defaultValue: false,
        description: "Required Input Field",
        table: {
          type: { summary: "boolean" },
        },
        control: {
          type: "boolean",
        },
      },      
      errorMessage: {  
        type: { name: 'string', required: false },
        defaultValue: "",
        description: 'errorMessage of Input Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },  
      
      onDataChange: {
          action: 'onDataChange',
          type: {name: 'function', required: true},
          description: '',
          table: {
              type: { summary: 'Function'},
          }
      },
  },
  parameters: {
    options: {showPanel: true },
      docs: {
          description: {
              component: 'InputMask component is used to enter input in a certain format such as numeric, date, currency, email and phone.' 
          }
      }
  },
} as ComponentMeta<typeof Core.HPHInputMask>;

const tempInputCoVslVoy: ComponentStory<typeof Core.HPHInputMask> = (args) => (
  <Core.HPHInputMask {...args} />
);


export const InputCoVslVoy = tempInputCoVslVoy.bind({});
tempInputCoVslVoy.args = {

};