
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";
export default {
    title: "component/Form Elements/Input Field",
    component: Core.ColorPicker,
    argTypes: { 
        label: {      
          type: { name: 'string', required: false },
          defaultValue: "label",
          description: 'label of Input Field',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },
        helpIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "helpIcon with tooltip-boolean property",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          toolTipText: {  
            type: { name: 'string', required: false },
            defaultValue: 'Help Icon',
            description: 'toolTipText of text area',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },    
        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          required: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Required Input Field",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'ColorPicker is an input component to select a color.'
            }
        }
    },
} as ComponentMeta<typeof Core.ColorPicker>;

const tempColorPicker: ComponentStory<typeof Core.ColorPicker> = (args) => (
    <Core.ColorPicker  {...args} />
);

export const InputColorPicker = tempColorPicker.bind({});
tempColorPicker.args = {

};



