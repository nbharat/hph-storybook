
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
    title: "component/Form Elements/Input Field",
    component: Core.CalendarDate,
    argTypes: {        
      label: {      
        type: { name: 'string', required: false },
        defaultValue: "label",
        description: 'label of Date Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
      width: {      
        type: { name: 'string', required: false },
        defaultValue: "200px",
        description: 'label of Input Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
        errorMessage: {  
          type: { name: 'string', required: false },
          defaultValue: "",
          description: 'errorMessage of Input Field date',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },           
      
        helpIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "helpIcon with tooltip-boolean property",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          toolTipText: {  
            type: { name: 'string', required: false },
            defaultValue: 'Date format dd/mm/yyyy',
            description: 'Date format dd/mm/yyyy',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },    
        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field date",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          required: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field date",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A Field that allows users to input a date manually.'
            }
        }
    },
} as ComponentMeta<typeof Core.CalendarDate>;

const tempCalendarDate: ComponentStory<typeof Core.CalendarDate> = (args) => (
    <Core.CalendarDate  {...args} />
);

export const InputFieldDate = tempCalendarDate.bind({});
tempCalendarDate.args = {

};



