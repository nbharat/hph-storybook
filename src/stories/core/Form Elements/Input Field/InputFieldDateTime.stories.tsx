
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
    title: "component/Form Elements/Input Field",
    component: Core.CalendarDateTime,
    argTypes: {        
      label: {      
        type: { name: 'string', required: false },
        defaultValue: "label",
        description: 'label of Date Field date time',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
        errorMessage: {  
          type: { name: 'string', required: false },
          defaultValue: "",
          description: 'errorMessage of Input Field date time',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        }, 
        helpIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "helpIcon with tooltip-boolean property",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          width: {      
            type: { name: 'string', required: false },
            defaultValue: "auto",
            description: 'label of Input Field',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          toolTipText: {  
            type: { name: 'string', required: false },
            defaultValue: 'Date time format dd/mm/yyyy hh:mm',
            description: 'Date time format dd/mm/yyyy hh:mm',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },    
        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable of Input date time",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          required: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field date time",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A Field that allows users to input a date and time manually.'
            }
        }
    },
} as ComponentMeta<typeof Core.CalendarDateTime>;

const tempCalendarDateTime: ComponentStory<typeof Core.CalendarDateTime> = (args) => (
    <Core.CalendarDateTime  {...args} />
);

export const InputFieldDateTime = tempCalendarDateTime.bind({});
tempCalendarDateTime.args = {

};



