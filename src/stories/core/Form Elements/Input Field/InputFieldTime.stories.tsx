
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
    title: "component/Form Elements/Input Field",
    component: Core.CalendarTime,
    argTypes: { 
        label: {      
          type: { name: 'string', required: false },
          defaultValue: "label",
          description: 'label of time Field',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },
        errorMessage: {  
          type: { name: 'string', required: false },
          defaultValue: "",
          description: 'errorMessage of Input Field',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },
        helpIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "helpIcon with tooltip-boolean property",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          width: {      
            type: { name: 'string', required: false },
            defaultValue: "auto",
            description: 'label of Input Field',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          toolTipText: {  
            type: { name: 'string', required: false },
            defaultValue: 'Time hh:mm 24 hours format',
            description: 'Time hh:mm 24 hours format',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },    
        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          required: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable Input Field",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A Field that allows users to input a time manually.'
            }
        }
    },
} as ComponentMeta<typeof Core.CalendarTime>;

const tempCalendarTime: ComponentStory<typeof Core.CalendarTime> = (args) => (
    <Core.CalendarTime  {...args} />
);

export const InputFieldTime = tempCalendarTime.bind({});
tempCalendarTime.args = {

};



