
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
    title: "component/Form Elements/Input Field",
    component: Core.HPHInputTextarea,
    argTypes: {
         
      label: {      
        type: { name: 'string', required: false },
        defaultValue: "label",
        description: 'label of text area',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      }, 
      width: {      
        type: { name: 'string', required: false },
        defaultValue: "200px",
        description: 'label of Input Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
      errorMessage: {  
        type: { name: 'string', required: false },
        defaultValue: "",
        description: 'errorMessage of text area',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
        helpIcon: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "helpIcon with tooltip-boolean property",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          toolTipText: {  
            
            type: { name: 'string', required: false },
            defaultValue: 'Help Icon',
            description: 'toolTipText of text area',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },

        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Disable textarea",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          required: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "Require textarea",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          autoResize: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "AutoResize textarea",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
        
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A Field that allows users to input text-based information and accept accept user inputs of more than one sentence.'
            }
        }
    },
} as ComponentMeta<typeof Core.HPHInputTextarea>;

const tempHPHInputTextarea: ComponentStory<typeof Core.HPHInputTextarea> = (args) => (
    <Core.HPHInputTextarea  {...args} />
);

export const InputTextarea = tempHPHInputTextarea.bind({});
tempHPHInputTextarea.args = {

};



