  import { ComponentStory, ComponentMeta } from "@storybook/react";
  import { object } from '@storybook/addon-knobs';
import { HPHRadio } from '../../../pages/RadioButton.page';

const Component = HPHRadio;
const OptionsList = [{  name: 'Option 1', key: 'A', inputId: 1 }, { name: 'Option 2', key: 'B', inputId: 2 }]; 

export default {
  title: "component/Form Elements/Radio Button",
  component: Component,
  argTypes: {
    options: {
      name: "Options",
      defaultValue: object("Options",OptionsList),
      description: "Options for the Radio Button",
      table: {
        type: { summary: "Array[Object]" },
      },
    },   
    disabled: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "disabled Active",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
    orientation: {
      type: { name: 'string', required: false },
      defaultValue: 'horizontal',
      description: 'Orientation of Radio Button',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: [ "horizontal", "vertical"]
      },
    },
  },
  parameters: {
    options: {showPanel: true },
    docs: {
      description: {
        component: 'Radio buttons are used in scenarios where users have a group of mutually exclusive choices and only a single selection from the group is permitted. Radio buttons are designed for mutually exclusive choices, not for multiple choices. In a session where radio buttons are employed, when a user chooses a new item, the previous choice will automatically be unselected/ deselected.-	The radio buttons can be laid out vertically or horizontally, depending on the use case and the UI structure.\n'  
      },
    },
  },
} as ComponentMeta<typeof Component>;

const tempRadio: ComponentStory<typeof Component> = (args) => (
  <Component {...args} />
);

export const Demo = tempRadio.bind({});
Demo.parameters= {
  options: {showPanel: false },
};
Demo.args = {
};

export const RadioButton = tempRadio.bind({});
RadioButton.args = {
  Options: object("Options",OptionsList)
};
