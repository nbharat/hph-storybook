
import { ComponentStory, ComponentMeta } from "@storybook/react";
import SingleSliderWithRangeSliderDemo from "../../../pages/SingleSliderWithRangeSliderDemo";
const Component = SingleSliderWithRangeSliderDemo;

export default {
  title: "component/Form Elements/Slider",
  component: Component,
  argTypes: {
    value: {      
      type: { name: 'string', required: false },
      defaultValue: "8",
      description: 'value of slider',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
    valuetxt: {      
      type: { name: 'string', required: false },
      defaultValue: "2,8",
      description: 'value of slider',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },  
    step:{
      defaultValue:1,
      description: 'value 1 of step',
      table: {
        type: { summary: 'string' },
      }       
    },    
    min: {
      name: "start",
      type: { name: 'number', required: false },
      defaultValue: 0,
      description: 'value of min',
      table: {
        type: { summary: 'number' },
      },
      control: {
        type: "number"
      },
    },    
    max: {
      name: "end",
      type: { name: 'number', required: false },
      defaultValue: 8,
      description: 'value of max',
      table: {
        type: { summary: 'number' },
      },
      control: {
        type: "number"
      },
    },
    orientation: {
      type: { name: 'string', required: false },
      defaultValue: 'horizontal',
      description: 'Orientation Position',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["horizontal", "vertical"]
      },
    },
    disabledtxt: {
      name: "hideInputBox",
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled Active Slider",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },  
    disabled: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled Active",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    }, 
    disabledInputBox: {
      name: "disabledInputBox",
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled Active Slider",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },   
    errorMessage: {  
      type: { name: 'string', required: false },
      defaultValue: "",
      description: 'value of slider',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
   
    label: {      
      type: { name: 'string', required: false },
      defaultValue: "label",
      description: 'value of slider',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    }, 
    onChange: {      
   
      description: 'onChange?(e: sliderChangeParams): retuen void   of slider',
      table: {
        type: { summary: 'string' },
      },
     
    }, 
  },
  parameters: {
    options: {showPanel: false },
    docs: {
      description: {
        component: 'Slider serves as a visual indicator for content items that are adjustable. It is often a control on the user interface which lets users select a value or a range from a fixed set of options. The slider’s horizontal track represents the range, and the knob indicates the current represented value. Users can increase or decrease the represented value by moving the knob along. The most basic form of a slider should include a label and a numeric input that also doubles as a display of the current represented value.'
      }
    }
  },
} as ComponentMeta<typeof Component>;

const DemoSlider: ComponentStory<typeof Component> = (args) => (
  <Component {...args} />
 
);
export const Demo = DemoSlider.bind({});



