
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
  title: "component/Form Elements/Slider",
  component: Core.SliderRange,
  argTypes: {
    valuetxt: {      
      type: { name: 'string', required: false },
      defaultValue: "2,8",
      description: 'value of slider',
      table: {
        type: { summary: 'Array Of value' },
      },
      control: {
        type: "text"
      },
    }, 
    step:{
      defaultValue:1,
      description: 'value 1 of step',
      table: {
        type: { summary: 'string' },
      }       
    },    
    min: {
      name: "start",
      type: { name: 'number', required: false },
      defaultValue: 0,
      description: 'value of min',
      table: {
        type: { summary: 'number' },
      },
      control: {
        type: "number"
      },
    },    
    max: {
      name: "end",
      type: { name: 'number', required: false },
      defaultValue: 8,
      description: 'value of max',
      table: {
        type: { summary: 'number' },
      },
      control: {
        type: "number"
      },
    },
    orientation: {
      type: { name: 'string', required: false },
      defaultValue: 'horizontal',
      description: 'Orientation Position',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["horizontal", "vertical"]
      },
    },
    disabledtxt: {
      name: "hideInputBox",
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled Active Slider",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },  
    disabled: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled Active",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    }, 
    disabledInputBox: {
      name: "disabledInputBox",
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabledtxt Active Slider",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },   
    errorMessage: {  
      type: { name: 'string', required: false },
      defaultValue: "",
      description: 'value of slider',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    },
   
    label: {      
      type: { name: 'string', required: false },
      defaultValue: "label",
      description: 'value of slider',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    }, 
    onChange: {      
   
      description: 'onChange?(e: sliderChangeParams): retuen void   of slider',
      
     
    }, 
  },
  parameters: {
    options: {showPanel: true },
    docs: {
      description: {
        component: 'Provides the option to select a range of numeric values intuitively by using two handles. A range slider usually fills the color between the handles to indicate selections.'
      }
    }
  },
} as ComponentMeta<typeof Core.SliderRange>;

export const RangeSlider: ComponentStory<typeof Core.SliderRange> = (args) => (
  <Core.SliderRange {...args}
  />
);



