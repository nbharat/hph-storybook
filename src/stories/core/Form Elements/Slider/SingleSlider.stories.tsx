
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

export default {
  title: "component/Form Elements/Slider",
  component: Core.SingleSlider,
  argTypes: {
    value: {     
      type: { name: 'number', required: false },
      defaultValue: 8,
      description: 'value of slider',
      table: {
        type: { summary: 'number' },
      },
      control: {
        type: "number"
      },
    },
    step:{      
      defaultValue:1,
      description: 'value 1 of step',
      table: {
        type: { summary: 'string' },
      }      
    },       
    min: {
      name: "start",
      type: { name: 'number', required: false },
      defaultValue: 0,
      description: 'value of min',
      table: {
        type: { summary: 'number' },
      },
      control: {
        type: "number"
      },
    },    
    max: {
      name: "end",
      type: { name: 'number', required: false },
      defaultValue: 8,
      description: 'value of max',
      table: {
        type: { summary: 'number' },
      },
      control: {
        type: "number"
      },
    },
    orientation: {
      type: { name: 'string', required: false },
      defaultValue: 'horizontal',
      description: 'Orientation Position',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: ["horizontal", "vertical"]
      },
    },
    disabled: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled Active Slider",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },  
    disabledtxt: {
      name: "hideInputBox",
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled Active Slider",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },  
    disabledInputBox: {
      name: "disabledInputBox",
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled Active Slider",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },      
    errorMessage: {      
      type: { name: 'string', required: false },
      defaultValue: "",
      description: 'value of slider',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    }, 
    label: {      
      type: { name: 'string', required: false },
      defaultValue: "label",
      description: 'label of slider',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "text"
      },
    }, 
    onChange: {      
   
      description: 'onChange?(e: sliderChangeParams): retuen void   of slider',
      table: {
        type: { summary: 'string' },
      },
     
    }, 
  },
  parameters: {
    options: {showPanel: true },
    docs: {
      description: {
        component: 'Single Sliders allow users to view and select a value from the range along a bar.'
      }
    }
  },
} as ComponentMeta<typeof Core.SingleSlider>;

export const SingleSlider: ComponentStory<typeof Core.SingleSlider> = (args) => (
  <Core.SingleSlider {...args}
  />
);



