
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { TimePickerComponent } from "../../../pages/TimePicker.page";

export default {
    title: "component/Form Elements/Time Picker",
    component: TimePickerComponent,
    argTypes: { 
      label: {      
        type: { name: 'string', required: false },
        defaultValue: "label",
        description: 'label of time Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
      errorMessage: {  
        type: { name: 'string', required: false },
        defaultValue: "",
        description: 'errorMessage of Input Field',
        table: {
          type: { summary: 'string' },
        },
        control: {
          type: "text"
        },
      },
      helpIcon: {
          type: { name: "boolean", required: false },
          defaultValue: false,
          description: "helpIcon with tooltip-boolean property",
          table: {
            type: { summary: "boolean" },
          },
          control: {
            type: "boolean",
          },
        },
        width: {      
          type: { name: 'string', required: false },
          defaultValue: "200px",
          description: 'label of Input Field',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },
        toolTipText: {  
          type: { name: 'string', required: false },
          defaultValue: 'Time hh:mm 24 hours format',
          description: 'Time hh:mm 24 hours format',
          table: {
            type: { summary: 'string' },
          },
          control: {
            type: "text"
          },
        },    
      disabled: {
          type: { name: "boolean", required: false },
          defaultValue: false,
          description: "Disable Input Field",
          table: {
            type: { summary: "boolean" },
          },
          control: {
            type: "boolean",
          },
        },
        required: {
          type: { name: "boolean", required: false },
          defaultValue: false,
          description: "Disable Input Field",
          table: {
            type: { summary: "boolean" },
          },
          control: {
            type: "boolean",
          },
        },      
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A Field that allows users to input a time manually.'
            }
        }
    },
} as ComponentMeta<typeof TimePickerComponent>;

const tempTimePicker: ComponentStory<typeof TimePickerComponent> = (args) => (
    <TimePickerComponent  {...args} />
);

export const Demo = tempTimePicker.bind({});
Demo.parameters= {
  options: {showPanel: false },
  layout: 'padded'
};
Demo.args = {};

export const TimePicker = tempTimePicker.bind({});
TimePicker.parameters= {
  layout: 'padded'
};
tempTimePicker.args = {};