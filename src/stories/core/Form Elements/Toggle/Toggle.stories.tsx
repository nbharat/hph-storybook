import { ComponentStory, ComponentMeta } from "@storybook/react";
import{ ToggleComponent } from '../../../pages/Toggle.page';

const Component = ToggleComponent;
export default {
    title: "component/Form Elements/Toggle",
    component: Component,
    argTypes: {
        disabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "disabled Active",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          label: {  
            type: { name: 'string', required: false },
            defaultValue: "Toggle",
            description: 'label of toggle',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          toolTipPlacement: {
            type: { name: 'string', required: false },
            defaultValue: 'bottom',
            description: 'Tooltip Position',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "select",
              options: ["top", "right", "left", "bottom","topLeft","topRight","bottomLeft","bottomRight"]
            },
          },
          tooltipDisabled: {
            type: { name: "boolean", required: false },
            defaultValue: false,
            description: "disabled Active",
            table: {
              type: { summary: "boolean" },
            },
            control: {
              type: "boolean",
            },
          },
          toolTipText: {      
            type: { name: 'string', required: false },
            defaultValue: "toggle",
            description: 'Tooltip text',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          }, 
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A toggle is commonly used to prompt users to choose between two mutually exclusive options and always have a default value. '  
            },
        },
        
        
    },
} as ComponentMeta<typeof Component>;

const tempToggle: ComponentStory<typeof Component> = (args) => (
    <Component {...args} />
);

export const Demo = tempToggle.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };
Demo.args = {
};

export const Toggle = tempToggle.bind({});
tempToggle.args = {

};
