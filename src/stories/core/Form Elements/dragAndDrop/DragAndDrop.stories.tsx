import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";
import { object } from '@storybook/addon-knobs';

const Component = Core.HPHDragAndDrop;

const selectedData = [
    { id: 1,order: 1, label: '3/4', value: '3/4',visible: true },
    { id: 2,order: 2, label: '6', value: '6',visible: true },
    { id: 3,order: 3, label: '7', value: '7', visible: true},
    ];

const unselectedData = [
    { id: 4,order: 4, label: '7/8', value: '7/8',visible: true },
    { id: 5,order: 5, label: '55', value: '55',visible: true },
    { id: 6,order: 6, label: '66', value: '66', visible: true },
    ];

export default {
    title: "component/Data Display/Drag And Drop ",
    component: Component,
    argTypes: {
        selectedOption: {
            name: "selectedData",
            defaultValue: object("selectedData",selectedData),
            description: "Selected tag options",
            table: {
              type: { summary: "Array[Object]" },
            },
        }, 
        unselectedOption: {
            name: "unselectedData",
            defaultValue: object("unselectedData",unselectedData),
            description: "Unselected tag options",
            table: {
              type: { summary: "Array[Object]" },
            },
        }, 
        width: {
            name: "width",
            type: { name: 'string', required: false },
            defaultValue: "500px",
            description: 'Width of the tag container',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "text"
            }
          },
        height: {
            name: "height",
            type: { name: 'string', required: false },
            defaultValue: "100px",
            description: 'Height of the tag container',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "text"
            }
          },
        orientation: {
            type: { name: 'string', required: false },
            defaultValue: 'horizontal',
            description: 'Orientation of Tags',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "select",
              options: [ "horizontal", "vertical"]
            },
        },
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'An avatar is an UI object that represents the users identity on a screen. The avatar usually contains a profile picture of the user.\n'+
              '<ul><li>The user’s status could be represented by a status dot, which sits at the bottom right of the avatar. The dot represents if the user is currently online or available.</li>\n'+
              '<li>For users who do not have a profile image uploaded, the avatar will be filled with his or her initials.</li>\n'+
              '<li>For avatars that contain only a character, the default background color is Ports Sea Blue. Users can be assigned other colors depending on the use case.</li>\n'
            }
        }
    },
} as ComponentMeta<typeof Component>;

const tempDragAndDrop: ComponentStory<typeof Component> = (args) => (
    <Component {...args} />
);

export const DragAndDrop = tempDragAndDrop.bind({});
DragAndDrop.args = {
    selectedOption: object("selectedData",selectedData),
    unselectedOption: object("unselectedData",unselectedData),
};

