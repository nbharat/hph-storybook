import { ComponentStory, ComponentMeta } from "@storybook/react";
import ContainerComp from "../../../pages/Container.page";

const Component = ContainerComp;

export default {
  title: "component/Layout Elements/Container",
  component: Component,
  argTypes: {
      width: {
        name: "width",
        type: { name: 'string', required: false },
        defaultValue: "400px",
        description: 'Width of the container',
        table: {
            type: { summary: 'string' },
        },
        control: {
            type: "text"
        }
      },
      height: {
        name: "height",
        type: { name: 'string', required: false },
        defaultValue: "400px",
        description: 'Height of the container',
        table: {
            type: { summary: 'string' },
        },
        control: {
            type: "text"
        }
      },
      theme: {
        name: "theme",
        type: { name: 'string', required: false },
        defaultValue: 'theme3',
        description: 'Container Theme',
        table: {
            type: { summary: 'string' },
        },
        control: {
            type: "select",
            options: ["theme1", "theme2", "theme3", "theme4"]
        },
      },
      borderRadius: {
        name: "borderRadius",
        type: { name: 'string', required: false },
        defaultValue: 'roundAll',
        description: 'Container Border Radius',
        table: {
            type: { summary: 'string' },
        },
        control: {
            type: "select",
            options: ["roundLeft", "roundRight", "roundAll", "roundNone"]
        },
      },
      onClick: {
        name: "onClick",
        description: 'This is to capture the click event.',
        table: {
          type: { summary: 'function' },
        },
      }
  },
  parameters: {
    options: { showPanel: true },
    docs: {
      description: {
          component: 'Here is a empty container. If user want to change the height and width, it can be added and there is a four themes which can change the border color of the container. \n'  
      },
    },
    backgrounds: {
      default: 'bg4',
      values: [
        {
          name: 'bg1',
          value: '#00aced',
        },
        {
          name: 'bg2',
          value: '#3b5998',
        },
        {
          name: 'bg3',
          value: '#4FBDBA',
        },
        {
          name: 'bg4',
          value: '#fff',
        },
      ],
    },
    
  }
} as ComponentMeta<typeof Component>;

const comp: ComponentStory<typeof Component> = (args) => {
  return <Component {...args} />;
};

export const Demo = comp.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };
Demo.args = {
};


export const Container = comp.bind({});

Container.args = {};
