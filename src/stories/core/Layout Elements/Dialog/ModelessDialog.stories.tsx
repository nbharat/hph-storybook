import { ComponentStory, ComponentMeta } from "@storybook/react";
import ModelessDialogPage  from '../../../pages/ModelessDialog.page';

const Component = ModelessDialogPage;

export default {
    title: "component/Layout Elements/Dialog",
    component: Component,
    argTypes: {
        label: {      
            type: { name: 'string', required: false },
            defaultValue: "Modal Overlay",
            description: 'Label of Dialog Modal',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
        subTitle: {      
            type: { name: 'string', required: false },
            defaultValue: "Sub Title",
            description: 'SubTitle of Dialog Modal',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
        },
        modalHeading:{      
            type: { name: 'string', required: false },
            defaultValue: "Title",
            description: 'ModalHeading of Dialog Modal',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
        },
        text:{      
            type: { name: 'string', required: false },
            defaultValue: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perspiciatis cupiditate nihil rem repellendus dicta ducimus mollitia? Voluptas doloremque iusto officiis.',
            description: 'text of Dialog Modal',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
        },
        positions:{
          type: { name: 'string', required: false },
          defaultValue: 'center',
          description: 'Dialog Position of Dialog Modal',
          table: {
              type: { summary: 'string' },
          },
          control: {
              type: "select",
              options: ['center' , 'top' , 'bottom' , 'left' , 'right' , 'top-left' , 'top-right' , 'bottom-left' , 'bottom-right']
          },
      },
      overlay:{
        name: "overlay",
        type: { name: "boolean", required: false },
        defaultValue: false,
        description: "Overlay of the Dialog Modal",
        table: {
          type: { summary: "boolean" },
        },
        control: {
          type: "boolean",
        },
      },
      resizable:{
        type: { name: "boolean", required: false },
        defaultValue: true,
        description: "Overlay of the Dialog Modal",
        table: {
          type: { summary: "boolean" },
        },
        control: {
          type: "boolean",
        },
      },
      draggable:{
        type: { name: "boolean", required: false },
        defaultValue: true,
        description: "Overlay of the Dialog Modal",
        table: {
          type: { summary: "boolean" },
        },
        control: {
          type: "boolean",
        },
      },
      disabledButton:{
        type: { name: "boolean", required: false },
        defaultValue: false,
        description: "Overlay of the Dialog Modal",
        table: {
          type: { summary: "boolean" },
        },
        control: {
          type: "boolean",
        },
      },
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A Dialog will popped up when user click on a button. The dialog will appear on top of the current page content.\n'
            }
        }
    },
} as ComponentMeta<typeof Component>;

const tempModelessDialog: ComponentStory<typeof Component> = (args) => (
    <Component {...args} />
);

export const ModelessDialog = tempModelessDialog.bind({});
tempModelessDialog.args = {
};

