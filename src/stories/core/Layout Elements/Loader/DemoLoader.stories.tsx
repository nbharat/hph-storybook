
import { ComponentStory, ComponentMeta } from "@storybook/react";
import LoaderDemo from "../../../pages/Loader.page"; 

const Component = LoaderDemo;

export default {
  title: "component/Layout Elements/Loading Indicators",
  component: Component,
  argTypes: {
    size: {
      type: { name: 'string', required: false },
      defaultValue: 'Small',
      description: '> Loading Indicator Size',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "select",
          options: [ "Small" , "Medium", "Large"]
      },
    },
  },
  parameters: {
      options: {showPanel: false },
  },
} as ComponentMeta<typeof Component>;

export const Demo: ComponentStory<typeof Component> = (args) => (
    <Component  {...args} />
);


