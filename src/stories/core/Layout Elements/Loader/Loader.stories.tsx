import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";


export default {
    title: "component/Layout Elements/Loading Indicators",
    component: Core.Loader,
    argTypes: {
      Indicator: {
        type: { name: 'string', required: false },
        defaultValue: 'Stripe',
        description: '> Loading Indicator',
        table: {
            type: { summary: 'string' },
        },
        control: {
            type: "select",
            options: [ "Spinner" , "Stripe"]
        },
      },
      size: {
        type: { name: 'string', required: false },
        defaultValue: 'Small',
        description: '> Loading Indicator Size',
        table: {
            type: { summary: 'string' },
        },
        control: {
            type: "select",
            options: [ "Small" , "Medium", "Large"]
        },
      },
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'The loading pattern will be in place when a certain piece of information or item takes an extended amount of time to process and be shown on the current screen.  \n'+
              '<ul><li>Since Veronica is a web-based application, skeleton states could be employed together with the component ‘loading indicator’ to visually communicate that data is loading/ the process is running at the background/ the screen is not frozen, in order to reduce users’ uncertainty.</li>\n'
            }
        }
    },
} as ComponentMeta<typeof Core.Loader>;

const tempLoader: ComponentStory<typeof Core.Loader> = (args) => (
    <Core.Loader {...args} />
);



export const LoadingIndicator = tempLoader.bind({});
tempLoader.args = {

};