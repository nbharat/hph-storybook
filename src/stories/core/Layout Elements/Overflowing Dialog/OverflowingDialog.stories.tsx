import { ComponentStory, ComponentMeta } from "@storybook/react";
import OverflowingDialogBox from '../../../pages/overflowingDialog.page';

const Component = OverflowingDialogBox;

export default {
    title: "component/Layout Elements/Overflowing Dialog",
    component: Component,
    argTypes: {
      boxPosition: {
            type: { name: 'string', required: false },
            defaultValue: 'right',
            description: 'Position of DialogBox',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "select",
                options: [ "right", "left" , "topLeft" , "topRight" , "bottomLeft" , "bottomRight" ]
            },
        },
        width: {
            name: "width",
            type: { name: 'string', required: false },
            defaultValue: "150px",
            description: 'Width of the Dialog Box',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "text"
            }
          },
        height: {
            name: "height",
            type: { name: 'string', required: false },
            defaultValue: "150px",
            description: 'Height of the Dialog Box',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "text"
            }
          },
        paddingTop: {
            name: "paddingTop",
            type: { name: 'string', required: false },
            defaultValue: "20px",
            description: 'Padding Top of the Dialog Box',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "text"
            }
          },
        paddingBottom: {
            name: "paddingBottom",
            type: { name: 'string', required: false },
            defaultValue: "20px",
            description: 'Padding Bottom of the Dialog Box',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "text"
            }
          },
        paddingLeft: {
            name: "paddingLeft",
            type: { name: 'string', required: false },
            defaultValue: "20px",
            description: 'Padding Left of the Dialog Box',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "text"
            }
          },
        paddingRight: {
            name: "paddingRight",
            type: { name: 'string', required: false },
            defaultValue: "20px",
            description: 'Padding Right of the Dialog Box',
            table: {
                type: { summary: 'string' },
            },
            control: {
                type: "text"
            }
          },
        children: {  
            type: { name: 'string', required: false },
            defaultValue: "<h1>Test</h1>",
            description: 'Children of Dialog Box',
            table: {
              type: { summary: 'HTMLElement' },
            },
            control: {
              type: "text"
            },
          },
      },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'Here is an empty Overflowing Dialog Box. User can change the box position(Left, Right, TopLeft, TopRight, BottomLeft, BottomRight) of the Overflowing dialog box. User can change the height, width and also change the padding(PaddingTop, PaddingRight, PaddingBottom, PaddingLeft) of the Overflowing dialog box. \n'  
            },
           
        },
        
        
    },
} as ComponentMeta<typeof Component>;

const tempDialog: ComponentStory<typeof Component> = (args) => (
    <Component {...args} />
);

export const Demo = tempDialog.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };


export const overflowingDialog = tempDialog.bind({});
tempDialog.args = {

};
