import { ComponentStory, ComponentMeta } from "@storybook/react";
import { object } from '@storybook/addon-knobs';
import OverflowingMenuComponent from "../../../pages/OverflowingMenu.page"

const Component = OverflowingMenuComponent;

let ItemSubMenu = [
   
       {
        label: "Option1",
      },
      {
        label: "Option2",
      },
      {
        label: "Option3",
      },
      {
        label: "Option4",
      },
    
      
    ];
  
export default {
  title: "component/Layout Elements/Overflowing Menu",
  component: Component,
  argTypes: {
    menuItem: {
      name: "menuItem",
      defaultValue: object("menuItem",ItemSubMenu),
      description: "MenuItems",
      table: {
        type: { summary: "Array[Object]" },
      },
    },   
    position: {
      type: { name: 'string', required: false },
      defaultValue: 'right',
      description: 'Tooltip Position',
      table: {
        type: { summary: 'string' },
      },
      control: {
        type: "select",
        options: [ "right", "left", "topLeft","topRight","bottomLeft","bottomRight"]
      },
    },   
    width: {
      name: "width",
      type: { name: 'string', required: false },
      defaultValue: "auto",
      description: 'Width of menu',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "text"
      }
    },
    height: {
      name: "height",
      type: { name: 'string', required: false },
      defaultValue: "auto",
      description: 'Height of menu',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "text"
      }
    },
    paddingTop: {
      name: "paddingTop",
      type: { name: 'string', required: false },
      defaultValue: "0.938rem",
      description: 'Padding Top',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "text"
      }
    },
    paddingRight: {
      name: "paddingRight",
      type: { name: 'string', required: false },
      defaultValue: "0rem",
      description: 'Padding Right',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "text"
      }
    },
    paddingBottom: {
      name: "paddingBottom",
      type: { name: 'string', required: false },
      defaultValue: "0.938rem",
      description: 'Padding Bottom',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "text"
      }
    },
    paddingLeft: {
      name: "paddingLeft",
      type: { name: 'string', required: false },
      defaultValue: "0rem",
      description: 'Padding Left',
      table: {
          type: { summary: 'string' },
      },
      control: {
          type: "text"
      }
    },
  },
  parameters: {
    options: { showPanel: true },
    docs: {
      description: {
        component:
        'Here is an empty Overflowing Menu. User can change the box position(Left, Right, TopLeft, TopRight, BottomLeft, BottomRight) of the Overflowing menu. User can change the height, width and also change the padding(PaddingTop, PaddingRight, PaddingBottom, PaddingLeft) of the Overflowing menu. \n'
      },
    },
  },
} as ComponentMeta<typeof Component>;

const comp: ComponentStory<typeof Component> = (args) => (
  <Component {...args} />
);
export const Demo = comp.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };



export const OverflowingMenu = comp.bind({});
OverflowingMenu.args = {
  menuItem: object("menuItem",ItemSubMenu),
};


