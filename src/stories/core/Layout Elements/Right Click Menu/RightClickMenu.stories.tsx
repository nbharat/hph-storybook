import { ComponentStory, ComponentMeta } from "@storybook/react";
import { object } from '@storybook/addon-knobs';
import RightClickMenuComp from "../../../pages/RightClickMenu.page";

const Component = RightClickMenuComp;
let ItemsWithIconSubMenu = [
  {
    label: "File",
    icon: "hph-icon Icon-alert-triangle",
    items: [
      {
        label: "SubFile 1",
      },
      {
        label: "SubFile 2",
      },
      {
        label: "SubFile 3",
      },
      {
        label: "SubFile 4",
      },
    ],
  },
  {
    label: "Bookmark",
    icon: "hph-icon Icon-add",
    items: [
      {
        label: "Bookmark1",
        icon: "hph-icon Icon-add",
      },
      {
        label: "Bookmark2",
        icon: "hph-icon Icon-add",
        items: [
          {
            label: "Bookmark3",
            icon: "hph-icon Icon-add",
          },
          {
            label: "Bookmark4",
            icon: "hph-icon Icon-add",
          },
          {
            label: "Bookmark5",
            icon: "hph-icon Icon-add",
          },
          {
            label: "Bookmark6",
            icon: "hph-icon Icon-add",
          },
          {
            label: "Bookmark7",
            icon: "hph-icon Icon-add",
          },
          {
            label: "Bookmark8",
            icon: "hph-icon Icon-add",
          },
          {
            label: "Bookmark9",
            icon: "hph-icon Icon-add",
          },
        ],
      },
    ],
  },
  {
    label: "View 1",
    icon: "hph-icon Icon-alert",
  },
  {
    label: "View 2",
    icon: "hph-icon Icon-alert",
  },
  {
    label: "View 3",
    icon: "hph-icon Icon-alert",
  },
  {
    label: "View 4",
    icon: "hph-icon Icon-alert",
  },
  {
    label: "View 5",
    icon: "hph-icon Icon-alert",
  },
  {
    label: "View 6",
    icon: "hph-icon Icon-alert",
  },
];

export default {
  title: "component/Layout Elements/Right Click Menu",
  component: Component,
  argTypes: {
    items: {
      name: "items",
      defaultValue: object("items",ItemsWithIconSubMenu),
      description: "MenuItems for the RightClickMenu",
      table: {
        type: { summary: "Array[Object]" },
      },
    },
    disabled: {
      type: { name: "boolean", required: false },
      defaultValue: false,
      description: "Disabled State",
      table: {
        type: { summary: "boolean" },
      },
      control: {
        type: "boolean",
      },
    },
  },
  parameters: {
    options: { showPanel: true },
    docs: {
      description: {
          component: 'The right click menu, also called a shortcut menu, is an option menu that will only be opened if the user performs a right click with the computer mouse on the menu. \n'  
      },
     
  },
    layout: 'fullscreen'
  }
} as ComponentMeta<typeof Component>;

const comp: ComponentStory<typeof Component> = (args) => {
  return <Component {...args} />;
};

export const Demo = comp.bind({});
Demo.parameters= {
  options: {showPanel: false },
  };
Demo.args = {
  items: object("items", ItemsWithIconSubMenu)
};

export const RightClickMenu = comp.bind({});

RightClickMenu.args = {
  items: object("items", ItemsWithIconSubMenu)
};
