import { ComponentStory, ComponentMeta } from "@storybook/react";
import { UIFrameWork } from "../../../pages/UIFrame.page";

export default {
  title: "component/Layout Elements/Ui Framework",
  component: UIFrameWork,
  argTypes: {
  },
  parameters: {
      options: {showPanel: false },
      layout: 'fullscreen',
      docs: {
          description: {
              component: 'The loading pattern will be in place when a certain piece of information or item takes an extended amount of time to process and be shown on the current screen.  \n'+
            '<ul><li>Since Veronica is a web-based application, skeleton states could be employed together with the component ‘loading indicator’ to visually communicate that data is loading/ the process is running at the background/ the screen is not frozen, in order to reduce users’ uncertainty.</li>\n'
          }
      }
  },
} as ComponentMeta<typeof UIFrameWork>;

const tempUiFramework: ComponentStory<typeof UIFrameWork> = (args) => (
  <UIFrameWork {...args} />
);

export const UiFramework = tempUiFramework.bind({});
tempUiFramework.args = {

};