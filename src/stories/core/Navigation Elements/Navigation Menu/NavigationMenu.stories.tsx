
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";
import { object } from '@storybook/addon-knobs';
import { NavigationMenuComponent } from "../../../pages/NavigationMenuDemo.page";

const StyleItems = [
  {
    marginRight: '1px'
  }
]

const Navigationitems = [
  {
     label:'Terminal \n Settings',
     items:[
      {
          label:'Berth',
          items:[
              {
              label:'ACT',
              },
              {
              label:'CHT',
              },
              {
                  label:'HIT4',
              },
              {
                  label:'HIT9',
              },
              {
                  label:'MTL1',
              },
              {
                  label:'MTL9',
              },
          ]
      },
      {
          label:'Quay  Crane',
      },
      {
          label:'Yard Crane',
      }
     ]
  },
  {
     label:'Operation Settings',
     items:[
      {
          label:'Line',
      },
      {
          label:'Service',
      },
      {
          label:'Vessel',
      },
     ]
  },
  {
    separator:true
  },
  {
     label:'Berth Plan',
     items:[
      {
          label:'Plan View',
      },
      {
          label:'Proforma View',
      }
     ]
  },
];

export default {
    title: "component/Navigation Elements/Navigation Menu",
    component: Core.NavigationMenu,
    argTypes: {
      items: {
        name: "Navigationitem",
        defaultValue: object("Navigationitem",Navigationitems),
        description: "MenuItems",
        table: {
          type: { summary: "Array[Object]" },
        },
      },
      breakLine: {
        type: { name: "boolean", required: false },
        defaultValue: false,
        description: "Break Line of Navigation Menu",
        table: {
          type: { summary: "boolean" },
        },
        control: {
          type: "boolean",
        },
      },
  popup: {
    type: { name: "boolean", required: false },
    defaultValue: false,
    description: "Defines if menu would displayed as a popup",
    table: {
      type: { summary: "boolean" },
    },
    control: {
      type: "boolean",
    },
  },
  style: {
    defaultValue: object('StyleItems', StyleItems),
    description: "Inline Style for Navigation Menu",
    table: {
      type: { summary: "Array[Object]" },
    },
  },
  className: {      
    type: { name: 'string', required: false },
    defaultValue: "Navigation",
    description: 'ClassName of Navigation Menu',
    table: {
      type: { summary: 'string' },
    },
    control: {
      type: "text"
    },
  }, 
  autoZIndex: {
    type: { name: "boolean", required: false },
    defaultValue: false,
    description: "Whether to automatically manage layering.",
    table: {
      type: { summary: "boolean" },
    },
    control: {
      type: "boolean",
    },
  },
  baseZIndex: {
    type: { name: 'number', required: false },
    defaultValue: '0',
    description: 'Base zIndex value to use in layering.',
    table: {
        type: { summary: 'number' },
    },
    control: {
        type: "number"
    },
},
  appendTo: {
    type: { name: 'string', required: false },
    defaultValue: 'self',
    description: 'AppendTo of Navigation Menu',
    table: {
        type: { summary: 'string' },
    },
    control: {
        type: "select",
        options: [ 'self' , 'HTMLElement' , 'undefined' , 'null' ]
    },
},
  transitionOptions:{
    defaultValue: object,
    description: "CSSTransition can be customized for Navigation Menu",
    table: {
      type: { summary: "Array[Object]" },
    },
  },
    },
    parameters: {
      options: { showPanel: true },
      docs: {
        description: {
          component:'The navigation menu is hidden inside the hamburger menu icon. The navigation menu will show when users hover on the hamburger menu icon. The navigation menu opens with a slide-out motion.'
        },
      },
      layout: 'fullscreen'
    }
    ,
} as ComponentMeta<typeof Core.NavigationMenu>;


const comp: ComponentStory<typeof Core.NavigationMenu> = (args) => (
    <Core.NavigationMenu {...args} />
);

const demoComp: ComponentStory<typeof NavigationMenuComponent> = (args) => (
  <NavigationMenuComponent {...args} />
);

export const Demo = demoComp.bind({});
Demo.parameters= {
  options: {showPanel: false },
};

export const NavigationMenu = comp.bind({});
NavigationMenu.args = {
  items: object("Navigationitem",Navigationitems),
  style:  object("StyleItems", StyleItems),
};




