import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Core } from "veronica-ui-component";

const Component = Core.HPHPaginator;

export default {
    title: "component/Navigation Elements/Pagination",
    component: Component,
    argTypes: {
        inputText: {      
            type: { name: 'string', required: false },
            defaultValue: "p.",
            description: 'Text for Input value',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          buttonText: {      
            type: { name: 'string', required: false },
            defaultValue: "Go",
            description: 'Text for Button',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            },
          },
          width: {
            type: { name: 'string', required: false },
            defaultValue: "750px",
            description: 'Width of the Pagination',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            }
          },
          height: {
            type: { name: 'string', required: false },
            defaultValue: "70px",
            description: 'height of the Pagination',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            }
          },
          totalRecords: {
            type: { name: 'string', required: false },
            defaultValue: "120",
            description: 'total Records of the Pagination',
            table: {
              type: { summary: 'number' },
            },
            control: {
              type: "text"
            }
          },
          row: {
            type: { name: 'string', required: false },
            defaultValue: "10",
            description: 'total number of row',
            table: {
              type: { summary: 'number' },
            },
            control: {
              type: "text"
            }
          },
          selectedText: {
            type: { name: 'string', required: false },
            defaultValue: "70",
            description: 'Number of the Selected Text',
            table: {
              type: { summary: 'number' },
            },
            control: {
              type: "text"
            }
          },
          Label: {
            type: { name: 'string', required: false },
            defaultValue: "Selected",
            description: 'Label for Selected Text',
            table: {
              type: { summary: 'string' },
            },
            control: {
              type: "text"
            }
          },
    },
    parameters: {
        options: {showPanel: true },
        docs: {
            description: {
                component: 'A pagination bar is used to split up/ segment content or data into several pages for clearer view and navigation. The pagination component serves as a control for navigating to the previous or next page. '
            }
        }
    },
} as ComponentMeta<typeof Component>;

const tempPagination: ComponentStory<typeof Component> = (args) => (
    <Component {...args} />
);

export const Demo = tempPagination.bind({});
Demo.parameters= {
        options: {showPanel: false },
        };
Demo.args = {
};

export const Pagination = tempPagination.bind({});
tempPagination.args = {

};

