import * as React from "react";
import { useState } from "react";
import { Core } from "veronica-ui-component";

interface Props {
    [x:string]: any;
}

const Component = Core.HPHCheckbox;

export const Check: React.FC<Props> = (props:any) => {

        const [ checked, setChecked] = useState<boolean>(false);
    return(
        <>
            <Component checked = {checked}
             onChange={(e:any)=>{setChecked(e.target.checked)} }
             {...props}
             />
        </>
    )
};