import * as React from "react";
import { Core } from "veronica-ui-component";

const Component = Core.Container;

interface Props {
    [x:string]: any;
}

const containerStyle: any = {
  root: {
    width: '100vw',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'auto'
  }
};

const Container: React.FC<Props> = (props) => {
  return (
    <div style={containerStyle.root}>
      <Component {...props} />
    </div>
  );
};

export default Container;