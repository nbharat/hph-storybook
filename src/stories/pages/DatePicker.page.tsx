import * as React from "react";
import { useState } from "react";
import { Core } from "veronica-ui-component";

interface Props {
    [x:string]: any;
}

const Component = Core.DatePicker;

export const DatePickerComponent: React.FC<Props> = (props:any) => {
    const [ value, setValue ] = useState<any>(props.value);
    return <Component {...props} value={value} onChange={(value: any) => setValue(value)} />;
};