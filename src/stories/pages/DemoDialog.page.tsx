import * as React from "react";
import ModalDialogPage from './ModalDialog.page';
import ModelessDialogPage  from './ModelessDialog.page';

const Dialoug1Component = ModalDialogPage;
const Dialoug2Component = ModelessDialogPage;

interface Props{
    [x:string]: any;
}

const SliderStyle: any = {
  container: {
    display: 'flex',
    alignItems: 'center'
  },
  sliderWrapper: {
    margin: "0 20px",
  },
};

const DialogDemo: React.FC<Props> = (props:any) => {
  return (
    <>
      <div style={SliderStyle.container}>
        <div style={SliderStyle.sliderWrapper}>  
         <Dialoug1Component draggable={true} 
                            label={'Modal Dialog'} 
                            modalHeading={'Title'}
                            overlay={true}
                            positions="center"
                            resizable={true}
                            subTitle={'Sub Title'}
                            disabledButton={false}
                            text={'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perspiciatis cupiditate nihil rem repellendus dicta ducimus mollitia? Voluptas doloremque iusto officiis'}/>
        </div>
        <div style={SliderStyle.sliderWrapper}>  
         <Dialoug2Component draggable={true} 
                            label={'Modeless Dialog'} 
                            modalHeading={'Title'}
                            overlay={false}
                            positions="center"
                            resizable={true}
                            subTitle={'Sub Title'}
                            disabledButton={false}
                            text={'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perspiciatis cupiditate nihil rem repellendus dicta ducimus mollitia? Voluptas doloremque iusto officiis'}/>

        </div>
      </div>
      
    </>
  );
};

export default DialogDemo;