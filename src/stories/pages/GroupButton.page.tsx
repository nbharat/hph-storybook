import * as React from "react";
import { useState } from "react";
import { Core } from "veronica-ui-component";

const Component = Core.GroupedButtons;
interface Props {
    [x:string]: any;
}
export const GroupedButtonComponent: React.FC<Props> = (props:any) => {
    const [value1, setValue1] = useState(null);
 
    return<Component {...props} value={value1} onChange={(e) => setValue1(e.value)} />;
};

