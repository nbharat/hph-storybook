import * as React from "react";
import { Core } from "veronica-ui-component";
import { object } from '@storybook/addon-knobs';

const DropdownComponent = Core.InputDropdown;

interface VirtualScrollerOptions {
    showLoader?: boolean,
    delay?: number,
    lazy?: boolean,
    onLazyLoad?: (e: any) => void,
    [x:string]: any
    label?: string

  }

interface Props{
    options?: any[],
    freeTextInput?: boolean | false,
    mode?: string | 'single',
    virtualScrollerOption?: VirtualScrollerOptions,
    onChange?: (e: any) => void,
    inputType?: string | 'freeText' | 'number' | 'text',
    field?: string | 'dropdownLabel' | 'tagLabel' | 'value',
    width? : string | '200px',
    [x:string]: any
}
const DropdownStyle: any = {
    container: {
      display: 'flex',
      alignItems: 'center'
    },
    dropdownWrapper: {
      margin: "0 50px",
    },
  };
  const options = Array.from({ length: 50 }).map((_, i) => ({ dropdownLabel: `Option ${i+1}`, value: `option${i+1}`, tagLabel: `O${i+1}`, icon: 'Icon-doc' }));

  const InputDropdownDemo: React.FC<Props> = (props:any) => {
    return (
    <>
      <div style={DropdownStyle.container}>
        <div style={DropdownStyle.dropdownWrapper}>  
        <DropdownComponent options={object("options",options)} freeTextInput={true} inputType={"freeText"}  mode={"single"} width={"250px"} field={"value"}  label={"Single Select with free text input	"} />       
        </div>
        
        <div style={DropdownStyle.dropdownWrapper}>  
        <DropdownComponent options={object("options",options)} freeTextInput={true} inputType={"freeText"} mode={"multiple"} width={"250px"} field={"value"}  label={"Multiple Select with free text input"} />
        </div>
      </div>
    </>
  );
};

export default InputDropdownDemo;

