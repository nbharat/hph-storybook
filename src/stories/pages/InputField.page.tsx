import * as React from "react";
import { Core } from "veronica-ui-component";

const InputFieldComponent = Core.InputField;
const InputDateField = Core.CalendarDate;
const InputDateTimeField = Core.CalendarDateTime;
const InputTimeField = Core.CalendarTime;
const InputTextAreaField = Core.HPHInputTextarea;
const InputMaskField = Core.HPHInputMask;

interface Props{
    placeholder?:string | '',
    type?:'text',
    label?: string,
    helpIcon?:boolean | false,
    required?:boolean | false,
    disabled?:boolean | false,
    errorMessage?:string,
    toolTipText?:string,
    width?:string | '200px',
    
}
const InputStyle: any = {
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      alignItems: 'center',
    },
    menuWrapper: {
      margin: "50px",
    },
  };

  const InputFieldDemo: React.FC<Props> = (props:any) => {
    return (
    <>
      <div style={InputStyle.container}>
         <div style={InputStyle.menuWrapper}>
          <InputDateField label="Input Date " disabled={false} required={false} helpIcon={false} errorMessage=''/>
        </div> 
        <div style={InputStyle.menuWrapper}>
          <InputTimeField label="Input Time" disabled={false} required={false} helpIcon={false} errorMessage=''/>
        </div> 
        <div style={InputStyle.menuWrapper}>
          <InputDateTimeField label="Input Date and Time" disabled={false} required={false} helpIcon={false} errorMessage=''/>
        </div>
        <div style={InputStyle.menuWrapper}>  
          <InputFieldComponent placeholder="Text" type="text" label="Input Field" 
          helpIcon={false} required={false} disabled={false} errorMessage='' 
          toolTipText="helper message" width="200px" />       
        </div>
          
        <div style={InputStyle.menuWrapper}>
          <InputTextAreaField label="Input TextArea" disabled={false} required={false} helpIcon={false} errorMessage=''/>
        </div>
      </div>      
    </>
  );
};

export default InputFieldDemo;