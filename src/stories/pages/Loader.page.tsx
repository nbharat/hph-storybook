import * as React from "react";
import { Core } from "veronica-ui-component";

const Component = Core.Loader;

interface Props{
    [x:string]: any;
}
const InputStyle: any = {
    container: {
      display: 'flex'
    },
    menuWrapper: {
      margin: "0 100px",
      position:"relative",
    },
  };

  const LoaderdDemo: React.FC<Props> = (props:any) => {
    return (
    <>
      <div style={InputStyle.container}>
         <div style={InputStyle.menuWrapper}>
             <Component Indicator='Spinner' {...props}/>
         </div>
         <div style={InputStyle.menuWrapper}>
             <Component Indicator='Stripe' {...props}/>
         </div>
      </div>
      
    </>
  );
};

export default LoaderdDemo;