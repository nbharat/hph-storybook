import * as React from "react";
import { Core } from "veronica-ui-component";

interface Props {
    [x:string]: any;
}

const Component = Core.Lozenges;

export const LozengesComponent: React.FC<Props> = (Props:any) => {
    return(<Component {...Props} />)
}
