import * as React from "react";
import { useState } from "react";
import { Core } from "veronica-ui-component";

const Component = Core.DialogModal;

interface Props{
    [x:string]: any;
}

  const ModalDialogPage: React.FC<Props> = (props:any) => {
    const [displayBasic, setDisplayBasic] = useState(false);
    const [position, setPosition] = useState('bottom-left');
  
    const dialogFuncMap:any = {
      displayBasic: setDisplayBasic,
    };
  
    const onClick = (name: any,display:any) => {
      dialogFuncMap[`${name}`](true);
  
      if (position) {
        setPosition(position);
      }
      if(displayBasic){
        setDisplayBasic(!displayBasic);
      }
    };
  
    const onHide = (name:any) => {
      dialogFuncMap[`${name}`](false);
    };
    return (
    <>
     <Component
     onFooterButtonClick = {() => onHide('displayBasic')} 
     onClick={() => onClick('displayBasic','center')}
     visible={displayBasic} 
     OnHide={() => onHide('displayBasic')}
     {...props}
     />
    </>
  );
};

export default ModalDialogPage;