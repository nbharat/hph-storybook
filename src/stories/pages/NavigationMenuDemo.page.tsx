import * as React from "react";
import { Core } from "veronica-ui-component";

interface Props {
    [x:string]: any;
}

const LeftNavigationitems = [
    {
       label:'Terminal \n Settings',
       items:[
        {
            label:'Berth',
            items:[
                {
                label:'ACT',
                },
                {
                label:'CHT',
                },
                {
                    label:'HIT4',
                },
                {
                    label:'HIT9',
                },
                {
                    label:'MTL1',
                },
                {
                    label:'MTL9',
                },
            ]
        },
        {
            label:'Quay  Crane',
        },
        {
            label:'Yard Crane',
        }
       ]
    },
    {
       label:'Operation Settings',
       items:[
        {
            label:'Line',
        },
        {
            label:'Service',
        },
        {
            label:'Vessel',
        },
       ]
    },
    {
      separator:true
    },
    {
       label:'Berth Plan',
       items:[
        {
            label:'Plan View',
        },
        {
            label:'Proforma View',
        }
       ]
    },
];

const RightNavigationitems = [
    {
       label:'Terminal Settings',
       items:[
        {
            label:'Berth',
            items:[
                {
                label:'ACT',
                },
                {
                label:'CHT',
                },
                {
                    label:'HIT4',
                },
                {
                    label:'HIT9',
                },
                {
                    label:'MTL1',
                },
                {
                    label:'MTL9',
                },
            ]
        },
        {
            label:'Quay  Crane',
        },
        {
            label:'Yard Crane',
        }
       ]
    },
    {
       label:'Operation Settings',
       items:[
        {
            label:'Line',
        },
        {
            label:'Service',
        },
        {
            label:'Vessel',
        },
       ]
    },
    {
      separator:true
    },
    {
       label:'Berth Plan',
       items:[
        {
            label:'Plan View',
        },
        {
            label:'Proforma View',
        }
       ]
    },
];


const Component = Core.NavigationMenu;

const NavigationMenuPageStyles = {
    container: {
        display: 'flex'
    },
    content: {
        flex: 1
    }
};

export const NavigationMenuComponent: React.FC<Props> = () => {
    return <div style={NavigationMenuPageStyles.container}>
        <div style={NavigationMenuPageStyles.content}>
            <Component items={LeftNavigationitems} />
        </div>

        <div style={NavigationMenuPageStyles.content}>
            <Component items={RightNavigationitems} />
        </div>
    </div>
}
