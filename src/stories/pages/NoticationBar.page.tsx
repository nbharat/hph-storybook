import * as React from "react";
import { useState, useEffect } from "react";
import { Core } from "veronica-ui-component";

const Component = Core.NotificationBar;

interface Props {
    [x:string]: any;
}

const containerStyle: any = {
  hide:{
  display: 'none'
  }
}

 const Notification: React.FC<Props> = (props:any) => {
    
    const [ counter, setCounter ] = useState(props.timer);
    const [ cross, setCross ]  = useState(true);

    useEffect(() => {
    var count = counter;
    if(count>0 && props.disable === false){
    var Countdown = setInterval(()=>{
      count--;
      setCounter(count);
    }, 1000);
  }
  return()=>
  clearInterval(Countdown);
  });
  
  return (
    <div id="notification">
      {cross && counter!==0 ?
      <Component 
      Time={counter}
      onClick={()=>{setCross(!cross)}}
      {...props} />
      : <div style={containerStyle.hide}/> }
    </div>
  );
  };
  
  export default Notification;
    
