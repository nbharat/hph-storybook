import * as React from "react";
import { useState } from "react";
import { Core } from "veronica-ui-component";

const Component = Core.NotificationCard;

interface Props {
    [x:string]: any;
}

export const NotificationToast: React.FC<Props> = (props) => {
  const [ isClose , setIsClose ] = useState(true);
  return (
    <>
    {isClose ?
      <Component onClick={()=>setIsClose(!isClose)} {...props} /> :
    ''}
    </>
  );
};
