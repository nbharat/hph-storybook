import * as React from "react";
import { Core } from "veronica-ui-component";
import {useState} from "react";

const Component = Core.HPHOverflowingMenu;
const ImgComponent = Core.SVGIcon;

interface Props {
    [x:string]: any,
    position?: | 'right' | 'left' | 'topLeft' |'topRight' | 'bottomLeft' | 'bottomRight',
}

const containerStyle: any = {
  root: {
  width: '30px',
  height: '30px',
  position: 'absolute',
  margin: '0',
  padding: '0',
  border: '0',
  background:'none',
  },
  icon:{
    position: 'absolute',
    width: '30px',
    height: '30px',
    left:'0px',
    top:'0px',
  },
  menu:{
    display: 'flex',
  }

};

const OverflowingMenuComponent: React.FC<Props> = (props) => {
  const [isShown, setIsShown] = useState(false);
  return (
    <button style={containerStyle.root}
    onMouseEnter={() => setIsShown(true)}
    onMouseLeave={() => setIsShown(false)}>
       
        <ImgComponent style={containerStyle.icon} fileName="Icon-setting"/>
        {isShown && (
      <Component style={containerStyle.menu} {...props} />
        )}
    </button>
  );
};

export default OverflowingMenuComponent;