import * as React from "react";
import { useState } from "react";
import { Core } from "veronica-ui-component";

interface Props {
    [x:string]: any;
}
const Component = Core.HPHRadio;

export const HPHRadio: React.FC<Props> = (Props:any) => {
    const [ checked, setChecked] = useState([]);
    return(<Component checked={checked} onChange={(e) =>{setChecked(e.value)}} {...Props}/>)
}


