import * as React from "react";
import { Core } from "veronica-ui-component";

const Component = Core.RightClickMenu;

interface Props {
  items: any[];
  disabled?: boolean;
}

const rightClickMenuStyle: any = {
  container: {
    display: 'flex',
    flex: 1,
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
    width:'100%',
  },
  div: {
    padding: "10px",
    border: "1px solid #ccc",
    width:"100%",
    height:"100%",
    display:"flex",
    alignItems:"center",
    justifyContent:"center"
  },
};
let items: any[] = [
  {
    label: "Items",

    items: [
      {
        label: "SubItem1",
      },
      {
        label: "SubItem2",
      },
      {
        label: "SubItem3",
        items: [
          {
            label: "SubItem1",
          },
          {
            label: "SubItem2",
          },
        ],
      },
      {
        label: "SubItem4",
      },
      {
        label: "SubItem5",
      },
      {
        label: "SubItem6",
      },
      {
        label: "SubItem7",
      },
      {
        label: "SubItem8",
      },
      {
        label: "SubItem9",
      },
    ],
  },
];

const RightClickMenu: React.FC<Props> = (props) => {
  const cm: any = React.createRef();
  return (
    <>
      <div style={rightClickMenuStyle.container} onContextMenu={(e) => cm.current && cm.current.show(e)}>
        <div style={rightClickMenuStyle.div}>
          RIGHT CLICK ME!!
        </div>
      </div>
      <Component element={cm} disabled={props.disabled} items={props.items ? props.items : items} />
    </>
  );
};

export default RightClickMenu;