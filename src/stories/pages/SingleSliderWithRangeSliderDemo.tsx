
import * as React from "react";
import { Core } from "veronica-ui-component";

const SingleSliderComponent = Core.SingleSlider;
const RangeSingleSliderComponent = Core.SliderRange;
type sliderOrientationType = 'horizontal' | 'vertical';
type sliderdisabled = true | false;
type sliderValueType = [number, number];

interface Props{
 
  id?: string,

  value?: sliderValueType,  
  min?: number, 
  max: number | 0, 
  orientation?: sliderOrientationType,
  step?: number,
  range?: boolean,
  style?: object,
  className?: string,
  disabledtxt?: sliderdisabled,
  disabled?: sliderdisabled,
  disabledInputBox?: sliderdisabled,
  errorMessage?: string,
  label?: string

}

const SliderStyle: any = {
  container: {
    display: 'flex',
    alignItems: 'center'
  },
  sliderWrapper: {
    margin: "0 20px",
  },
};

const SingleSliderWithRangeSliderDemo: React.FC<Props> = (props:any) => {
  return (
    <>
      <div style={SliderStyle.container}>
        <div style={SliderStyle.sliderWrapper}>  
          <SingleSliderComponent value={8} step={1} max={8} min={0} orientation="horizontal" disabled={false} disabledtxt={false} disabledInputBox={false} errorMessage="" label={"Single Slider"} />       
        </div>
        <div style={SliderStyle.sliderWrapper}>  
         <RangeSingleSliderComponent value={[2,8]} step={1} max={8} min={0} orientation="horizontal" disabled={false} disabledtxt={false} disabledInputBox={false} errorMessage="" label={"Range Slider"}/>
        </div>
      </div>
      
    </>
  );
};

export default SingleSliderWithRangeSliderDemo;