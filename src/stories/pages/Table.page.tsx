import * as React from "react";
import { Core } from "veronica-ui-component";

interface Props {
    [x:string]: any;
}

const containerStyle: any = {
    root: {
      width: '100vw',
      height: '100vh',
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      overflow: 'auto'
    }
  };

const Component = Core.HPHTable;

export const TableComponent: React.FC<Props> = (Props:any) => {
    return(
        <div style={containerStyle.root}>
            <Component {...Props}/>
        </div>
    )
};
