import * as React from "react";
import { Core } from "veronica-ui-component";

interface Props {
    [x:string]: any;
}

const Component = Core.Tags;

export const TagsComponent: React.FC<Props> = (Props:any) => {
    const [ show, setShow] = React.useState(true);
    
    const removeHandler = () => setShow(false);

    return <div>{show && <Component {...Props} onRemove={removeHandler} />}</div>
}
