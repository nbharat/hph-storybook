import * as React from "react";
import { Core } from "veronica-ui-component";

interface Props {
    [x:string]: any;
}

const Component = Core.Tags;

const TagsContainerStyles: any = {
    display: 'flex',
    width: 525,
    border: '1px dashed #002E6D',
    borderRadius: 4,
    padding: 10,
    flexWrap:'wrap',
    justifyContent :'space-between',
    margin: '0 10px',
};

const TagsStyles: any = {
    margin: '5px 0px'
};

export const TagsDemoComponent: React.FC<Props> = (Props:any) => {
    const options = Array.from({ length: 6 }).map((_, i) => ({ 
        id: i+1,
        label: `Option ${i+1}`,
        value: `option${i+1}`,
        icon: 'Icon-doc',
        visible: true,
        order: i+1
      }));
      
    const [tags, setTags] = React.useState(options);
   
    const removeHandler = (item: any) => {

        const updatedTags: any = tags.map(tag => {
            if (tag.id === item.id) {
                tag.visible = false;
            }
            return tag;
       });

        setTags(updatedTags);
    };

    const onDragOver = (ev: any) => {
        ev.preventDefault();
    }

    const onDrop = (ev: any) => {
       let id = Number(ev.dataTransfer.getData("id"));

       const dragTag: any = tags.find((tag) => tag.id === id);
       const dropTag: any = tags.find((tag) => tag.id === Number(ev.currentTarget.id));
   
       const dragTagOrder = dragTag?.order;
       const dropTagOrder = dropTag?.order;
   
       const newTagState = tags.map((tag) => {
        if (tag.id === id) {
           tag.order = dropTagOrder;
        }

        if (tag.id === Number(ev.currentTarget.id)) {
           tag.order = dragTagOrder;
        }
        return tag;
       });
   
       setTags(newTagState);
    }

    const onDragStart = (ev: any, id: any) => {
        ev.dataTransfer.setData("id", id);
    }

    return <div style={{ display: 'flex', width: 'auto', justifyContent: 'space-between'}}>
        <div 
            style={TagsContainerStyles}
            onDragOver={(ev: any) => onDragOver(ev)}        
        >
            {tags.sort((a, b) => a.order - b.order).map(
                (item : any, i: number) => item.visible && <Component 
                    key={i}
                    {...item}
                    draggable={true}
                    remove={true}
                    enableIcon={true}
                    style={TagsStyles}
                    onRemove={() => removeHandler(item)}
                    onDragStart = {(e: any) => onDragStart(e, item.id)}
                    onDrop={(e: any) => onDrop(e)}
                />
            )}
        </div>
    </div>
}
