import * as React from "react";
import { useState } from "react";
import { Core } from "veronica-ui-component";

const Component = Core.HPHToggle;

interface Props {
    [x:string]: any;
}
export const ToggleComponent: React.FC<Props> = (props:any) => {
        const [ checked, setChecked] = useState<boolean>(false);
    return<Component {...props} checked = {checked} onChange={(e:any)=>{setChecked(e.value)} }/>;
};

