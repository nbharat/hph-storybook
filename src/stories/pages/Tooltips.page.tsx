import * as React from "react";
import { Core } from "veronica-ui-component";

interface Props {
    [x:string]: any;
}

const Component = Core.Tooltips;

export const TooltipsComponent: React.FC<Props> = (Props:any) => {
    return(<Component style={{ display: 'block' }} {...Props} />)
}
