import * as React from "react";
import { useState } from "react";
import { Core } from "veronica-ui-component";

const Component = Core.UiFramework;

interface Props {
    [x:string]: any;
}

const horizontalCheckbox = [{ name: 'Adhoc Call', key: 'A' }, { name: 'Dual Voyage', key: 'M' }, { name: 'Phase In/Out', key: 'P' }];
const verticalCheckbox = [{ name: 'Special Arrangement', key: 'A' }, { name: 'Previous/Next Port', key: 'B' }, { name: 'Remarks', key: 'C' }, { name: 'LOA', key: 'D' }, { name: 'DS/LD Figures', key: 'E' }, { name: 'Outreach', key: 'F' }, { name: 'Derrick', key: 'G' }, { name: 'Start/End Bollard', key: 'H' }, { name: 'BridgePosition', key: 'I' }, { name: 'ETB Delay', key: 'J' }, { name: 'Proforma Delay', key: 'K' }];
const Table1Columns = [
  {
    dataType: 'text',
    disabled: true,
    field: 'equation',
    filter: true,
    filterPlaceholder: 'Enter equation...',
    header: 'Equation',
    id: 1,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
  {
    dataType: 'text',
    disabled: false,
    field: 'loa',
    filter: true,
    filterPlaceholder: 'Enter LOA...',
    header: 'Loa(m)',
    id: 2,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
  {
    dataType: 'text',
    disabled: false,
    field: 'clearance',
    filter: true,
    filterPlaceholder: 'Enter Date...',
    header: 'Clearance(m)',
    id: 3,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
];
const Table2Columns = [
  {
    dataType: 'text',
    disabled: true,
    field: 'equation',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Equation',
    id: 1,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
  {
    dataType: 'text',
    disabled: false,
    field: 'hours',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Hours',
    id: 2,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
  {
    dataType: 'color',
    disabled: false,
    field: 'color',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Color',
    id: 3,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: false,
  },
];
const Table3Columns = [
  {
    dataType: 'text',
    disabled: true,
    field: 'type',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Type',
    id: 1,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
  {
    dataType: 'text',
    disabled: true,
    field: 'equation',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Equation',
    id: 1,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
  {
    dataType: 'text',
    disabled: false,
    field: 'hours',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Hours',
    id: 2,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
  {
    dataType: 'color',
    disabled: false,
    field: 'color',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Color',
    id: 3,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: false,
  },
];
const Table4Columns = [
  {
    dataType: 'text',
    disabled: true,
    field: 'type',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Type',
    id: 1,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
  {
    dataType: 'text',
    disabled: false,
    field: 'condition',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Condition',
    id: 1,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: true,
  },
  {
    dataType: 'color',
    disabled: false,
    field: 'color',
    filter: true,
    filterPlaceholder: 'Please type...',
    header: 'Color',
    id: 3,
    showAddButton: false,
    showClearButton: false,
    showFilterMatchModes: false,
    showFilterMenuOptions: false,
    sortable: false,
  },
];
export const UIFrameWork: React.FC<Props> = (props) => {
  const [isActive, setActive] = useState(false);
  const [ radio1, setRadio1] = useState([]);    
  const [ radio2, setRadio2] = useState([]);    
  const [ radio3, setRadio3] = useState([]);    
  const [check, setCheck] = useState<any>([]);
  const [groupButtonValue, setGroupButtonValue] = useState('4');
  const [groupButtonValue1, setGroupButtonValue1] = useState('A');
  const [ tableColumns, setTableColumns] = useState<any>([]); 

  const onCheckChange = (e: { value: any, checked: boolean }) => {
    let Check = [...check];
    if (e.checked) {
      Check.push(e.value);
    } else {
      for (let i = 0; i < Check.length; i++) {
        const selectedCategory = Check[i];
        if (selectedCategory.key === e.value.key) {
          Check.splice(i, 1);
          break;
        }
      }
    }
    setCheck(Check);
  };

  const ToggleSidebarOpen = (opt:any) => {
    console.log(
      opt.target.id 
    );
  if (opt.target.id === 'Table1'){
    setTableColumns(Table1Columns) ;
    } else if (opt.target.id === 'Table2'){
      setTableColumns(Table2Columns) ;
      } else if (opt.target.id === 'Table3'){
        setTableColumns(Table3Columns) ;
        } else if (opt.target.id === 'Table4'){
          setTableColumns(Table4Columns) ;
          }
    setActive(true);
  };
  const ToggleSidebarClose = () => {
    setActive(false);
  };
  
    return (
<Component
isActive={isActive}
ToggleSidebarOpen={ToggleSidebarOpen}
ToggleSidebarClose={ToggleSidebarClose}
radio1={radio1}
radio2={radio2}
radio3={radio3}
onRadioChange1={(e:any) =>setRadio1(e.value)}
onRadioChange2={(e:any) =>setRadio2(e.value)}
onRadioChange3={(e:any) =>setRadio3(e.value)}
horizontalCheckbox={horizontalCheckbox}
verticalCheckbox={verticalCheckbox}
onCheckChange={onCheckChange}
check={check}
groupButtonValue={groupButtonValue}
onGroupButtonChange={(e:any)=>setGroupButtonValue(e.value)}
groupButtonValue1={groupButtonValue1}
onGroupButtonChange1={(e:any)=>setGroupButtonValue1(e.value)}
tableColumns={tableColumns}
Table1Columns={Table1Columns}
Table2Columns={Table2Columns}
Table3Columns={Table3Columns}
Table4Columns={Table4Columns}
/>
);
};
