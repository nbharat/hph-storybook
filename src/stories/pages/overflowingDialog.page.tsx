import * as React from "react";
import { Core } from "veronica-ui-component";
import { useState } from "react";

const DialogComponent = Core.DialogBox;
const ImgComponent = Core.SVGIcon;

interface Props {
   [x:string]: any;
}

const containerStyle: any = {
div:{
  position: 'relative',
  width: '30px',
  height: '60px',
  display: 'flex',
  border: 'none',
  background: 'none',
  cursor: 'pointer',

},
icon:{
    position: 'absolute',
    width: '30px',
    height: '30px',
    left: '0.5rem',
  },
};

const OverflowingDialogBox: React.FC<Props> = (props) => {
  const [isShown, setIsShown] = useState(false);
  return (
      <button style={containerStyle.div} 
        onMouseEnter={() => setIsShown(true)}
        onMouseLeave={() => setIsShown(false)}
      >
        <ImgComponent style={containerStyle.icon}
         fileName="Icon-setting" />
         {isShown && 
        <DialogComponent
        {...props} />}
        </button>
  );
};

export default OverflowingDialogBox;